import React, { Component } from "react";
import { Nav, Container, Row, Col, Sonnet, Navbar } from "react-bootstrap";

import Bar from "../../layout/Navbar/navbar";
import TabNav from "../../layout/Tab/tabNav";
import TabBar from "../../layout/Tab/tabBar";
import ManageMember from "../ManageMember/manageMember";

export default class HomeAdmin extends Component {
  render() {
    return (
      <div>
        <TabNav />
        <TabBar />
      </div>
    );
  }
}
