import React, { Component } from "react";
import { Row, Col, Form, Button, Table } from "react-bootstrap";
import "rsuite/lib/styles/index.less";
import treeOne from "../../assets/images/category/tree1.jpg";
import treeTwo from "../../assets/images/category/tree2.jpg";
import treeThree from "../../assets/images/category/tree3.jpg";
import treeFour from "../../assets/images/category/tree4.jpg";
import treeFive from "../../assets/images/category/tree5.jpg";
import treeSix from "../../assets/images/category/tree6.jpg";
import treeSale from "../../assets/images/category/tree7.jpg";
import "./manageOrder.scss";

export default class ManageOrder extends Component {
  render() {
    return (
      <div style={{ margin: "0 10%" }}>
        <Row style={{ margin: "0px", padding: "20px 0", alignItems: "center", display: "flex" }}>
          <Col style={{ padding: "0px" }}>
            <h1>จัดการการสั่งซื้อ</h1>
          </Col>
        </Row>
        <Row style={{ margin: "0px", padding: "0px" }}>
          <Table striped bordered hover>
            <thead>
              <tr>
                <th>สถานะคำสั่งซื้อ</th>
                <th>รหัสคำสั่งซื้อ</th>
                <th>ผู้สั่ง</th>
                <th>ตรวจสอบ</th>
                <th>ลบคำสั่งซื้อ</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>Mark</td>
                <td>Otto</td>
                <td>@mdo</td>
                <td>
                  <Button
                    onClick={() => {
                      window.location.href = "/admin/order/check";
                    }}
                    className="button-custom-order"
                    variant="warning"
                  >
                    ตรวจสอบ
                  </Button>
                </td>
                <td>
                  <Button className="button-custom-order" variant="danger">
                    ลบ
                  </Button>
                </td>
              </tr>
            </tbody>
          </Table>
        </Row>
      </div>


      // <div className="container-order">
      //   <section className="first-main-order">
      //     <div>
      //       <h1>จัดการการสั่งซื้อ</h1>
      //     </div>
      //   </section>
      //   <section className="display-order">
      //     <div>
      //       <Table striped bordered hover className="table-container-order">
      //         <thead>
      //           <tr>
      //             <th>สถานะคำสั่งซื้อ</th>
      //             <th>รหัสคำสั่งซื้อ</th>
      //             <th>ผู้สั่ง</th>
      //             <th>ตรวจสอบ</th>
      //             <th>ลบคำสั่งซื้อ</th>
      //           </tr>
      //         </thead>
      //         <tbody>
      //           <tr>
      //             <td>Mark</td>
      //             <td>Otto</td>
      //             <td>@mdo</td>
      //             <td>
      //               <Button
      //                 onClick={() => {
      //                   window.location.href = "/admin/order/check";
      //                 }}
      //                 className="button-custom-order"
      //                 variant="warning"
      //               >
      //                 ตรวจสอบ
      //               </Button>
      //             </td>
      //             <td>
      //               <Button className="button-custom-order" variant="danger">
      //                 ลบ
      //               </Button>
      //             </td>
      //           </tr>
      //           <tr>
      //             <td>Jacob</td>
      //             <td>Thornton</td>
      //             <td>@fat</td>
      //             <td>
      //               <Button className="button-custom-order" variant="warning">
      //                 ตรวจสอบ
      //               </Button>
      //             </td>
      //             <td>
      //               <Button className="button-custom-order" variant="danger">
      //                 ลบ
      //               </Button>
      //             </td>
      //           </tr>
      //           <tr>
      //             <td colSpan="2">Larry the Bird</td>
      //             <td>@twitter</td>
      //             <td>
      //               <Button className="button-custom-order" variant="warning">
      //                 ตรวจสอบ
      //               </Button>
      //             </td>
      //             <td>
      //               <Button className="button-custom-order" variant="danger">
      //                 ลบ
      //               </Button>
      //             </td>
      //           </tr>
      //         </tbody>
      //       </Table>
      //     </div>
      //   </section>
      // </div>
    );
  }
}
