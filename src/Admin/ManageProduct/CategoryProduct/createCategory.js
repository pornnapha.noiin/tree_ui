import React, { Component } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import { Row, Col, Form, Button, Table } from "react-bootstrap";
import "rsuite/lib/styles/index.less";
import "./saveCategory.scss";
import Bar from "../../../layout/Navbar/navbar";

export default class CreateCategory extends Component {
  render() {
    return (
      <div>
        <div className="container-product">
          <Bar />
          <section className="first-main-product-edit">
            <div>
              <h1>จัดการข้อมูลหมวดหมู่สินค้า</h1>
            </div>
            <div className="flex-end-button-cate">
              <Button
                onClick={() => {
                  // localStorage.setItem("value", "manageProduct");
                  window.location.href = "/admin/category/save";
                }}
                className="button-custom-product"
                variant="info"
              >
                เพิ่มหมวดหมู่สินค้า
              </Button>
            </div>
          </section>
          <section className="display-product-edit">
            <div>
              <Table striped bordered hover className="table-container-product">
                <thead>
                  <tr>
                    <th>รหัสหมวดหมู่สินค้า</th>
                    <th>ชื่อหมวดหมู่สินค้า</th>
                    <th>แก้ไขหมวดหมู่</th>
                    <th>ลบหมวดหมู่</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>1</td>
                    <td>1</td>
                    <td>
                      <Button className="button-custom-product" variant="info">
                        แก้ไข
                      </Button>
                    </td>
                    <td>
                      <Button
                        className="button-custom-product"
                        variant="danger"
                      >
                        ลบ
                      </Button>
                    </td>
                  </tr>
                  <tr>
                    <td>2</td>
                    <td>1</td>
                    <td>
                      <Button className="button-custom-product" variant="info">
                        แก้ไข
                      </Button>
                    </td>
                    <td>
                      <Button
                        className="button-custom-product"
                        variant="danger"
                      >
                        ลบ
                      </Button>
                    </td>
                  </tr>
                  <tr>
                    <td>3</td>
                    <td>1</td>
                    <td>
                      <Button
                        onClick={() => {
                          localStorage.setItem("value", "manageProduct");
                          window.location.href = "/admin/product/home";
                        }}
                        className="button-custom-product"
                        variant="info"
                      >
                        แก้ไข
                      </Button>
                    </td>
                    <td>
                      <Button
                        className="button-custom-product"
                        variant="danger"
                      >
                        ลบ
                      </Button>
                    </td>
                  </tr>
                </tbody>
              </Table>
            </div>
          </section>
        </div>
      </div>
    );
  }
}
