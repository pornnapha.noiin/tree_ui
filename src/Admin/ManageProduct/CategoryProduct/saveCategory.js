import React, { Component } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import { Row, Col, Form, Button, Table, Card } from "react-bootstrap";
import "rsuite/lib/styles/index.less";
import "./saveCategory.scss";
import Bar from "../../../layout/Navbar/navbar";
import logoTreeSale from "../../../assets/images/mytree.jpg";
import { POST } from "../../../Config/service";
import Swal from 'sweetalert2'
import { Link } from 'react-router-dom'

export default class SaveCategory extends Component {
  constructor(props) {
    super(props)
    this.state = {
      input: {},
      filePlaceholder: null
    }
  }

  componentDidMount = () => {
    let { type } = this.props
    if (type === "edit") this.getDetailCategory()
  }

  getDetailCategory = async () => {
    let { productType } = this.props
    let formData = new FormData()
    try {
      formData.append("productType", productType)
      let res = await POST("/category_product/list", formData)
      this.setState({ input: res.data[0] })
    } catch (error) {
      console.log(`error`, error)
    }
  }

  onCreateCategory = async () => {
    let { input } = this.state
    let formData = new FormData()
    try {
      for (let key in input) { formData.append(key, input[key]) }
      let res = await POST('/category_product/save', formData);
      if (res.status === 1) {
        Swal.fire({
          title: 'สำเร็จ',
          text: `หมวดหมู่ ${res.data.categoryName} ถูกเพิ่มแล้ว`,
          icon: 'success',
          confirmButtonText: 'เสร็จสิ้น',
        }).then((active) => {
          if (active.isConfirmed) window.location.reload()
        })
      } else if (res.status === 0) {
        Swal.fire({
          title: 'ไม่สำเร็จ',
          text: `หมวดหมู่ ${res.data.categoryName} มีแล้ว`,
          icon: 'info',
          confirmButtonText: 'ตกลง',
        })
      } else {
        Swal.fire({
          title: 'ไม่สำเร็จ',
          text: `ระบบเกิดข้อผิดพลาด`,
          icon: 'error',
          confirmButtonText: 'ตกลง',
        })
      }
    } catch (error) {
      console.log(`error : `, error)
    }
  }

  onEditCategory = () => {
    alert("EDIT")
  }

  handleChangeText = ({ name, value }) => {
    let { input } = this.state
    input[name] = value
    this.setState({ input })
  }

  handleChangeNumber = ({ name, value }) => {
    let { input } = this.state
    input[name] = Number(value)
    this.setState({ input })
  }

  handleChangeFile = ({ name, files }) => {
    let { input, filePlaceholder } = this.state
    if (files[0]) {
      input[name] = files[0]
      filePlaceholder = files[0].name
      this.setState({ input, filePlaceholder })
    }
  }

  render() {

    let { type } = this.props
    let { filePlaceholder, input } = this.state

    switch (type) {
      case "create":
        return (
          // <div>
          //   <div className="container-product">
          //     <Bar />
          //     <section className="first-main-product-edit">
          //       <div>
          //         <h1>บันทึกข้อมูลหมวดหมู่สินค้า</h1>
          //       </div>
          //     </section>
          //     <section className="display-product-edit">
          //       <div className="body-code-gradient-login">
          //         <div className="center-container">
          <Card className="card-inside __modal-add">
            <Card.Body>
              <div className="card-container">
                <img alt="" src={logoTreeSale} width="45" height="45" />
              </div>
              <div className="card-container">
                <text className="text-card">
                  บันทึกข้อมูลหมวดหมู่สินค้า
                </text>
              </div>
              <Form
                // onSubmit={this.onCreateCategory}
                style={{ paddingTop: "10px" }}
              // noValidate
              >
                <Row className="margin-form">
                  <Col>
                    <Form.Group
                      onChange={(e) => this.handleChangeText(e.target)}
                      as={Col}
                      controlId="formBasicName"
                    >
                      <Form.Label className="label-text">
                        ชื่อหมวดหมู่สินค้า
                      </Form.Label>
                      <Form.Control
                        className="padding-form"
                        size="sm"
                        type="text"
                        name="categoryName"
                        placeholder="ชื่อหมวดหมู่สินค้า"
                      // onChange={this.handlenameChange}
                      />
                      {/* <div className="invalid-feedback d-block">
                      {this.state.errorname}
                    </div> */}
                    </Form.Group>
                  </Col>
                  {/* <Col>
                          <Form.Group
                            onChange={(e) => this.handleChangeNumber(e.target)}
                            as={Col}
                            controlId="formBasicName"
                          >
                            <Form.Label className="label-text">
                              ชนิดหมวดหมู่
                            </Form.Label>
                            <Form.Control
                              className="padding-form"
                              size="sm"
                              type="number"
                              name="productType"
                              placeholder="ชนิดหมวดหมู่"
                            />
                          </Form.Group>
                        </Col> */}
                </Row>
                <Row className="margin-form">
                  <Col>
                    <Form.Group
                      onChange={this.onChange}
                      as={Col}
                    // controlId="formBasicSurname"
                    >
                      <Form.Label className="label-text">
                        รูปภาพหมวดหมู่
                      </Form.Label>
                      <div className="upload">
                        <Form.Control
                          className="padding-form"
                          size="sm"
                          type="text"
                          name="file"
                          placeholder={filePlaceholder ? filePlaceholder : "เลือกรูปภาพหมวดหมู่"}
                          disabled={true}
                        />
                        <input accept=".png,.jpg" type="file" name="file" className="file-upload" onChange={(e) => this.handleChangeFile(e.target)} />
                      </div>
                      {/* <div className="invalid-feedback d-block">
                      {this.state.errorsurname}
                    </div> */}
                    </Form.Group>
                  </Col>
                </Row>
                <div className="card-container">
                  {/* {this.state.statusDateTime === 1 ? ( */}
                  {/* <Link to="/admin/home">
              <Button
                // onClick={() => {
                //   window.location.href = "/admin/home";
                // }}
                size="sm"
                variant="danger"
                style={{ marginTop: "5px", width: "7rem" }}
              >
                กลับ
              </Button>
            </Link> */}

                  {/* ) : null} */}
                  &nbsp;
                  <Button
                    size="sm"
                    variant="info"
                    // type="submit"
                    style={{ marginTop: "5px", width: "7rem" }}
                    onClick={this.onCreateCategory}
                  >
                    เพิ่ม
                  </Button>
                </div>
              </Form>
            </Card.Body>
          </Card>
          //         {/* </div>
          //       </div>
          //     </section>
          //  </div>
          // </div> */}

        );
      case "edit":
        return (
          <Card className="card-inside __modal-add">
            <Card.Body>
              <div className="card-container">
                <img alt="" src={logoTreeSale} width="45" height="45" />
              </div>
              <div className="card-container">
                <text className="text-card">
                  แก้ไขข้อมูลหมวดหมู่สินค้า
                </text>
              </div>
              <Form style={{ paddingTop: "10px" }} >
                <Row className="margin-form">
                  <Col>
                    <Form.Group onChange={(e) => this.handleChangeText(e.target)} as={Col} controlId="formBasicName">
                      <Form.Label className="label-text"> ชื่อหมวดหมู่สินค้า </Form.Label>
                      <Form.Control className="padding-form" size="sm" type="text" value={input?.categoryName} name="categoryName" placeholder="ชื่อหมวดหมู่สินค้า" />
                    </Form.Group>
                  </Col>
                </Row>
                <Row className="margin-form">
                  <Col>
                    <Form.Group onChange={this.onChange} >
                      <Form.Label className="label-text"> รูปภาพหมวดหมู่</Form.Label>
                      <div className="upload">
                        <Form.Control className="padding-form" size="sm" type="text" name="file" placeholder={filePlaceholder ? filePlaceholder : input?.categoryPic || "เลือกรูปภาพหมวดหมู่"} disabled={true} />
                        <input accept=".png,.jpg" type="file" name="file" className="file-upload" onChange={(e) => this.handleChangeFile(e.target)} />
                      </div>
                    </Form.Group>
                  </Col>
                </Row>
                <div className="card-container">
                  &nbsp;
                  <Button size="sm" variant="info" style={{ marginTop: "5px", width: "7rem" }} onClick={this.onEditCategory} >
                    บันทึก
                  </Button>
                </div>
              </Form>
            </Card.Body>
          </Card>
        );
    }
  }
}
