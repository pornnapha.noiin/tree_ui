import React, { Component } from "react";
import { Row, Col, Form, Button, Table, Modal } from "react-bootstrap";
import "rsuite/lib/styles/index.less";
import "./manage.scss";

export default class Manage extends Component {
  constructor(props) {
    super(props)
    this.state = {
      productType: localStorage.getItem("productType"),
      categoryName: localStorage.getItem("categoryName"),
      isModalCreateProduct: false,
    }
  }

  onModalChange = (typeName, active, productType) => {
    if (typeName === "createProduct") this.setState({ isModalCreateProduct: active })
  }

  createProductView = () => {
    return (
      <h1>เพิ่มสินค้า</h1>
    )
  }

  render() {

    let { productType, categoryName, isModalCreateProduct } = this.state

    return (
      <div style={{ margin: "0 10%" }}>

        <Row style={{ margin: "0px", padding: "20px 0", alignItems: "center", display: "flex" }}>
          <Col style={{ padding: "0px" }}>
            <h1>{`จัดการสินค้า > หมวดหมู่ > ${categoryName}`} </h1>
          </Col>
          <Col lg={2} md={3} sm={4} style={{ textAlign: "end", padding: "0px" }}>
            <Button onClick={() => this.onModalChange("createProduct", true)} variant="success">
              เพิ่มสินค้า
            </Button>
          </Col>
        </Row>
        <Row style={{ margin: "0px", padding: "0px" }}>
          <Table striped bordered hover >
            <thead>
              <tr>
                <th>รหัสสินค้า</th>
                <th>ชื่อสินค้า</th>
                <th>แก้ไขข้อมูล</th>
                <th>ลบข้อมูล</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>1</td>
                <td>1</td>
                <td>
                  <Button
                    onClick={() => {
                      localStorage.setItem("valueEdit", "edit");
                      window.location.href = "/admin/product/edit";
                    }}
                    className="button-custom-product"
                    variant="info"
                  >
                    แก้ไข
                  </Button>
                </td>
                <td>
                  <Button className="button-custom-product" variant="danger">
                    ลบ
                  </Button>
                </td>
              </tr>
              <tr>
                <td>2</td>
                <td>1</td>
                <td>
                  <Button className="button-custom-product" variant="info">
                    แก้ไข
                  </Button>
                </td>
                <td>
                  <Button className="button-custom-product" variant="danger">
                    ลบ
                  </Button>
                </td>
              </tr>
            </tbody>
          </Table>
        </Row>

        {/* Modal Here!! */}
        <Modal centered size={'lg'} show={isModalCreateProduct} onHide={() => this.onModalChange("createProduct", false)}>
          <Modal.Header closeButton>
            <Modal.Title>เพิ่มข้อมูลสินค้า</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            {this.createProductView()}
          </Modal.Body>
        </Modal>

      </div>


      // <div className="container-product">
      //   <section className="first-main-product">
      //     <div>
      //       <h1>จัดการสินค้า</h1>
      //     </div>
      //   </section>
      //   <section className="display-product">
      //     <div>
      //       <Table striped bordered hover className="table-container-product">
      //         <thead>
      //           <tr>
      //             <th>รหัสสินค้า</th>
      //             <th>ชื่อสินค้า</th>
      //             <th>แก้ไขข้อมูล</th>
      //             <th>ลบข้อมูล</th>
      //           </tr>
      //         </thead>
      //         <tbody>
      //           <tr>
      //             <td>1</td>
      //             <td>1</td>
      //             <td>
      //               <Button
      //                 onClick={() => {
      //                   localStorage.setItem("valueEdit", "edit");
      //                   window.location.href = "/admin/product/edit";
      //                 }}
      //                 className="button-custom-product"
      //                 variant="info"
      //               >
      //                 แก้ไข
      //               </Button>
      //             </td>
      //             <td>
      //               <Button className="button-custom-product" variant="danger">
      //                 ลบ
      //               </Button>
      //             </td>
      //           </tr>
      //           <tr>
      //             <td>2</td>
      //             <td>1</td>
      //             <td>
      //               <Button className="button-custom-product" variant="info">
      //                 แก้ไข
      //               </Button>
      //             </td>
      //             <td>
      //               <Button className="button-custom-product" variant="danger">
      //                 ลบ
      //               </Button>
      //             </td>
      //           </tr>
      //         </tbody>
      //       </Table>
      //     </div>
      //   </section>
      // </div>
    );
  }
}
