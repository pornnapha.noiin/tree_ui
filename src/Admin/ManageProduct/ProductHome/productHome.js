import React, { Component } from "react";
import TabNav from "../../../layout/Tab/tabNav";
import TabProduct from "../Tab/tabProduct";

export default class ProductHome extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: localStorage.getItem("value"),
    };
    console.log("value:", this.state.value);
  }
  render() {
    return (
      <div>
        <TabNav />
        <TabProduct />
      </div>
    );
  }
}
