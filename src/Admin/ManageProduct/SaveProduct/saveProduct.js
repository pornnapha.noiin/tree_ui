import React, { Component } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import { Row, Col, Form, Button, Table } from "react-bootstrap";
import "rsuite/lib/styles/index.less";
import "./saveProduct.scss";
import Bar from "../../../layout/Navbar/navbar";

export default class SaveProduct extends Component {
  render() {
    return (
      <div>
        <div className="container-product">
          <Bar />
          <section className="first-main-product-edit">
            <div>
              <h1>บันทึกข้อมูลสินค้า</h1>
            </div>
          </section>
          <section className="display-product-edit">
            <div>
              <Table striped bordered hover className="table-container-product">
                <thead>
                  <tr>
                    <th>รหัสสินค้า</th>
                    <th>ชื่อสินค้า</th>
                    <th>แก้ไขข้อมูล</th>
                    <th>ลบข้อมูล</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>1</td>
                    <td>1</td>
                    <td>
                      <Button className="button-custom-product" variant="info">
                        แก้ไข
                      </Button>
                    </td>
                    <td>
                      <Button
                        className="button-custom-product"
                        variant="danger"
                      >
                        ลบ
                      </Button>
                    </td>
                  </tr>
                  <tr>
                    <td>2</td>
                    <td>1</td>
                    <td>
                      <Button className="button-custom-product" variant="info">
                        แก้ไข
                      </Button>
                    </td>
                    <td>
                      <Button
                        className="button-custom-product"
                        variant="danger"
                      >
                        ลบ
                      </Button>
                    </td>
                  </tr>
                  <tr>
                    <td>3</td>
                    <td>1</td>
                    <td>
                      <Button
                        onClick={() => {
                          localStorage.setItem("value", "manageProduct");
                          window.location.href = "/admin/product/home";
                        }}
                        className="button-custom-product"
                        variant="info"
                      >
                        แก้ไข
                      </Button>
                    </td>
                    <td>
                      <Button
                        className="button-custom-product"
                        variant="danger"
                      >
                        ลบ
                      </Button>
                    </td>
                  </tr>
                </tbody>
              </Table>
            </div>
          </section>
        </div>
      </div>
    );
  }
}
