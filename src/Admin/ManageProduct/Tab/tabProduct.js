import React, { Component } from "react";
import Tab from "react-bootstrap/Tab";
import "bootstrap/dist/css/bootstrap.min.css";
import "./tabProduct.scss";
import { Nav, Container, Row, Col, Sonnet, Navbar } from "react-bootstrap";
import ManageMember from "../../ManageMember/manageMember";
import ManageOrder from "../../ManageOrder/manageOrder";
import ManageProduct from "../manageProduct";
import Manage from "../Manage/manage";

export default class TabProduct extends Component {
  constructor(props) {
    super(props);
    this.state = {
      listMenu: "",
      checkActive: false,
      value: "product",
      valueManage: localStorage.getItem("value"),
    };
    this.handleClick = this.handleClick.bind(this);
    console.log("value:", this.state.valueManage);
  }

  handleClick(event) {
    this.setState({
      value: event.target.id,
    });
    console.log("****value****", this.state.value);
  }

  render() {
    return (

      <Row style={{ margin: "0" }}>
        <Col sm={2} style={{ padding: "0", height: "90vh", backgroundColor: "rgba(248,249,250,1)" }}>
          <Nav.Item>
            <Nav.Link onClick={this.handleClick} id="product">
              สินค้า
            </Nav.Link>
          </Nav.Item>
          <Nav.Item>
            <Nav.Link onClick={this.handleClick} id="register">
              สมาชิก
            </Nav.Link>
          </Nav.Item>
          <Nav.Item>
            <Nav.Link onClick={this.handleClick} id="order">
              คำสั่งซื้อ
            </Nav.Link>
          </Nav.Item>
        </Col>
        <Col sm={10} style={{ padding: "0", height: "auto", backgroundColor: "white" }}>
          {this.state.valueManage === "manageProduct" ? (
            <div>
              <Manage />
            </div>
          ) : this.state.value === "order" ? (
            <div>
              <ManageOrder />
            </div>
          ) : this.state.value === "product" ? (
            <div>
              <ManageProduct />
            </div>
          ) : null}
        </Col>
      </Row >

      // <div>
      //   <>
      //     <div>
      //       <Row className="container-bar">
      //         <Col>
      //           <div>
      //             <Navbar className="nav-fix-tab" bg="light" variant="light">
      //               <Row style={{ width: "100%" }}>
      //                 <Col sm={3} className="col-sm-3">
      //                   <Nav variant="pills" className="flex-column">
      //                     <Nav.Item>
      //                       <Nav.Link onClick={this.handleClick} id="product">
      //                         สินค้า
      //                       </Nav.Link>
      //                     </Nav.Item>
      //                     <Nav.Item>
      //                       <Nav.Link onClick={this.handleClick} id="register">
      //                         สมาชิก
      //                       </Nav.Link>
      //                     </Nav.Item>
      //                     <Nav.Item>
      //                       <Nav.Link onClick={this.handleClick} id="order">
      //                         คำสั่งซื้อ
      //                       </Nav.Link>
      //                     </Nav.Item>
      //                   </Nav>
      //                 </Col>
      //               </Row>
      //             </Navbar>
      //           </div>
      //         </Col>
      //         <Col>
      //           {this.state.valueManage === "manageProduct" ? (
      //             <div>
      //               <Manage />
      //             </div>
      //           ) : this.state.value === "order" ? (
      //             <div>
      //               <ManageOrder />
      //             </div>
      //           ) : this.state.value === "product" ? (
      //             <div>
      //               <ManageProduct />
      //             </div>
      //           ) : null}
      //         </Col>
      //       </Row>
      //     </div>
      //   </>
      // </div>
    );
  }
}
