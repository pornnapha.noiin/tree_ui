import React, { Component } from "react";
import { Link, withRouter } from "react-router-dom";

import { Row, Col, Form, Button, Table, Container, Modal } from "react-bootstrap";
import { POST } from "../../Config/service";
import { SaveCategory } from "Admin";
import Swal from "sweetalert2";

import "rsuite/lib/styles/index.less";
import "./manageProduct.scss";

class ManageProduct extends Component {
  constructor(props) {
    super(props);
    this.state = {
      categoryArr: [
        "ไม้ยืนต้น",
        "ไม้เลื้อย",
        "ไม้ผล",
        "ไม้แนวรั้ว",
        "ไม้ทรงพุ่ม",
        "ไม้ปลูกในบ้าน",
      ],
      data: [],
      search: {},
      isModalCreateCategory: false,
      isModalEditCategory: false,
      productType: 0
    };
  }

  componentDidMount = () => {
    this.getALlCategoryProduct()
  }

  getALlCategoryProduct = async () => {
    try {
      let res = await POST('/category_product/list_all');
      this.setState({ data: res.data })
      // console.log(`data : `, this.state.data)
    } catch (error) {
      console.log(`error : `, error)
    }
  }

  toProductHome = (productType, categoryName) => {
    localStorage.setItem("value", "manageProduct")
    localStorage.setItem("productType", productType)
    localStorage.setItem("categoryName", categoryName)
    this.props.history.push('/admin/product/home')
  }

  /* --- On Click --- */

  onDeleteCategory = async (id, name) => {
    let formData = new FormData()
    formData.append("categoryProductId", id)
    try {
      Swal.fire({
        title: 'ลบหมวดหมู่',
        text: `ต้องการลบหมวดหมู่ ${name} หรือไม่ ?`,
        icon: 'question',
        showDenyButton: true,
        denyButtonText: 'ยกเลิก',
        confirmButtonText: 'ยืนยัน',
      }).then(async (active) => {
        if (active.isConfirmed) {
          let res = await POST("/category_product/delete", formData);
          if (res.status === 1) {
            this.getALlCategoryProduct()
          } else {
            Swal.fire({
              title: 'ไม่สำเร็จ',
              text: `ระบบเกิดข้อผิดพลาด`,
              icon: 'error',
              confirmButtonText: 'ตกลง',
            })
          }
        }
      })
    } catch (error) {
      console.log(`error : `, error)
    }
  }

  onClickSearch = () => {
    let { data, search } = this.state
    // let newSearch = data?.filter(el => (search.categoryName ? (search.categoryName === el.categoryName) : el))
    let newSearch = data?.filter(el => (search.categoryName ? (el.categoryName === search.categoryName) : el))
    // let str = 'Hello mama'
    // let a = str.startsWith("Hello")
    // console.log(`a`, a)
    this.setState({ newSearch })
    console.log(`newSearch`, newSearch)

    const people = [
      { first: 'John', last: 'Doe', year: 1991, month: 6 },
      { first: 'Jane', last: 'Doe', year: 1990, month: 9 },
      { first: 'Jahn', last: 'Deo', year: 1986, month: 1 },
      { first: 'Jone', last: 'Deo', year: 1992, month: 11 },
      { first: 'Jhan', last: 'Doe', year: 1989, month: 4 },
      { first: 'Jeon', last: 'Doe', year: 1992, month: 2 },
      { first: 'Janh', last: 'Edo', year: 1984, month: 7 },
      { first: 'Jean', last: 'Edo', year: 1981, month: 8 },
    ];

    var filtered = people.filter(p => String(p.year).startsWith('198'));

    console.log(filtered);
  }

  onModalChange = (typeName, active, productType) => {
    if (typeName === "createCategory") this.setState({ isModalCreateCategory: active })
    if (typeName === "editCategory") this.setState({ isModalEditCategory: active, productType: productType })
  }

  /* --- On Change --- */
  handleChangeSearch = ({ name, value }) => {
    let { search } = this.state
    search[name] = value
    this.setState({ ...search })
  }

  /* --- Modal VIew --- */

  createCategoryView = () => {
    return (
      <SaveCategory id="createCategory" type="create" />
    )
  }
  editCategoryView = () => {
    let { productType } = this.state
    return (
      <SaveCategory id="editCategory" type="edit" productType={productType} />
    )
  }

  render() {

    let { data, isModalCreateCategory, isModalEditCategory, newSearch } = this.state

    return (
      <div style={{ margin: "0 10%" }}>
        <Row style={{ margin: "0px", padding: "20px 0", alignItems: "center", display: "flex" }}>
          <Col style={{ padding: "0px" }}>
            <h1>หมวดหมู่สินค้า</h1>
          </Col>
          <Col style={{ padding: "0px" }}>
            <div style={{ display: "flex" }}>

              <Form.Control onChange={(e) => this.handleChangeSearch(e.target)} className="padding-form" size="sm" type="text" name="categoryName" placeholder="กรอกชื่อหมวดหมู่" />
              <Button onClick={this.onClickSearch} variant="primary">
                ค้นหา
              </Button>
            </div>

          </Col>
          <Col lg={2} md={3} sm={4} style={{ textAlign: "end", padding: "0px" }}>
            {/* <Link to="/admin/category/save">
              <Button>
                เพิ่มหมวดหมู่
              </Button>
            </Link> */}
            <Button onClick={() => this.onModalChange("createCategory", true)} variant="success">
              เพิ่มหมวดหมู่
            </Button>
          </Col>
        </Row>
        <Row style={{ margin: "0px", padding: "0px" }}>
          <Table striped bordered hover >
            <thead>
              <tr>
                <th>รหัสหมวดหมู่</th>
                <th>ชื่อหมวดหมู่</th>
                <th>จัดการสินค้า</th>
                <th className="__center">แก้ไข</th>
                <th className="__center">ลบหมวดหมู่</th>
                {/* <th>ลบหมวดหมู่</th> */}
              </tr>
            </thead>
            <tbody>
              {(newSearch ? newSearch : data)?.map((el, i) => (
                <tr key={i}>
                  <td>{el.productType}</td>
                  <td>{el.categoryName}</td>
                  <td>
                    <Button onClick={() => this.toProductHome(el.productType, el.categoryName)} className="button-custom-product" variant="info">
                      จัดการ
                    </Button>
                  </td>
                  <td className="__center" >
                    <Button onClick={() => this.onModalChange("editCategory", true, el.productType)} className="button-custom-product" variant="warning">
                      แก้ไข
                    </Button>
                  </td>
                  <td className="__center">
                    <Button onClick={() => this.onDeleteCategory(el.categoryProductId, el.categoryName)} className="button-custom-product" variant="danger">
                      ลบ
                    </Button>
                  </td>
                  {/* <td>
                       <Button
                         className="button-custom-product"
                         variant="danger"
                       >
                         ลบหมวดหมู่
                       </Button>
                     </td> */}
                </tr>
              ))}
            </tbody>
          </Table>
        </Row>

        {/* Modal Here!! */}
        <Modal centered size={'lg'} show={isModalCreateCategory} onHide={() => this.onModalChange("createCategory", false)}>
          <Modal.Header closeButton>
            <Modal.Title>บันทึกข้อมูลหมวดหมู่สินค้า</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            {this.createCategoryView()}
          </Modal.Body>
        </Modal>

        <Modal centered size={'lg'} show={isModalEditCategory} onHide={() => this.onModalChange("editCategory", false)}>
          <Modal.Header closeButton>
            <Modal.Title>แก้ไขข้อมูลหมวดหมู่สินค้า</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            {this.editCategoryView()}
          </Modal.Body>
        </Modal>

      </div >
      // <div className="container-product">
      //   <section className="first-main-product">
      //     <div>
      //       <h1>จัดการสินค้า</h1>
      //     </div>
      //     <div className="flex-end-button">

      //       <Link to="/admin/category/save">
      //         <Button
      //           // onClick={() => {
      //           //   localStorage.setItem("value", "manageProduct");
      //           //   window.location.href = "/admin/category/save";
      //           // }}
      //           className="button-custom-product"
      //           variant="info"
      //         >
      //           เพิ่มหมวดหมู่
      //         </Button>
      //       </Link>
      //     </div>
      //   </section>


      //   <section className="display-product">
      //     <div>
      //       <Table striped bordered hover className="table-container-product">
      //         <thead>
      //           <tr>
      //             <th>หมวดหมู่สินค้า</th>
      //             <th>จัดการสินค้า</th>
      //             <th>เพิ่มสินค้า</th>
      //             {/* <th>ลบหมวดหมู่</th> */}
      //           </tr>
      //         </thead>
      //         <tbody>
      //           {this.state.categoryArr.map((index) => (
      //             <tr>
      //               <td>{index}</td>
      //               <td>
      //                 <Button
      //                   onClick={() => {
      //                     localStorage.setItem("value", "manageProduct");
      //                     window.location.href = "/admin/product/home";
      //                   }}
      //                   className="button-custom-product"
      //                   variant="info"
      //                 >
      //                   จัดการ
      //                 </Button>
      //               </td>
      //               <td>
      //                 <Button
      //                   onClick={() => {
      //                     window.location.href = "/admin/product/save";
      //                   }}
      //                   className="button-custom-product"
      //                   variant="warning"
      //                 >
      //                   เพิ่ม
      //                 </Button>
      //               </td>
      //               {/* <td>
      //                 <Button
      //                   className="button-custom-product"
      //                   variant="danger"
      //                 >
      //                   ลบหมวดหมู่
      //                 </Button>
      //               </td> */}
      //             </tr>
      //           ))}
      //         </tbody>
      //       </Table>
      //     </div>
      //   </section>
      // </div>
    );
  }
}

export default withRouter(ManageProduct)
