import React, { Component } from "react";
import { Row, Col, Form, Button, Table, Dropdown } from "react-bootstrap";
import "rsuite/lib/styles/index.less";
import treeOne from "../../assets/images/category/tree1.jpg";
import treeTwo from "../../assets/images/category/tree2.jpg";
import treeThree from "../../assets/images/category/tree3.jpg";
import treeFour from "../../assets/images/category/tree4.jpg";
import treeFive from "../../assets/images/category/tree5.jpg";
import treeSix from "../../assets/images/category/tree6.jpg";
import treeSale from "../../assets/images/category/tree7.jpg";
import "./report.scss";

export default class OrderReport extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: "day",
    };
    this.handleChange = this.handleChange.bind(this);
  }
  handleChange(event) {
    console.log("event", event);
    this.setState({
      value: event.target.value,
    });
    console.log("value", this.state.value);
  }
  render() {
    return (
      <div style={{ margin: "0 10%" }}>
        <Row style={{ margin: "0px", padding: "20px 0", alignItems: "center", display: "flex" }}>
          <Col style={{ padding: "0px" }}>
            <h1>รายงานการซื้อ-ขายสินค้า</h1>
          </Col>
          <Col sm={2} style={{ justifyContent: 'end', display: "flex", padding: "0" }}>
            <Form.Select
              className="select-container"
              aria-label="Default select example"
              value={this.state.value}
              onChange={this.handleChange}
            >
              <option>เลือกช่วงเวลา</option>
              <option value="day">ภายในวัน</option>
              <option value="month">ภายในเดือน</option>
              <option value="year">ภายในปี</option>
            </Form.Select>
          </Col>
          <Col sm={2} style={{ padding: "0" }} >
            {this.state.value === "year" ? (
              <div style={{ justifyContent: 'end', display: "flex" }}>
                <Form.Select
                  className="select-container"
                  aria-label="Default select example"
                >
                  <option>เลือกปี</option>
                  <option value="1">2561</option>
                  <option value="2">2562</option>
                  <option value="3">2563</option>
                </Form.Select>
              </div>
            ) : this.state.value === "month" ? (
              <div style={{ justifyContent: 'end', display: "flex" }}>
                <Form.Select
                  className="select-container"
                  aria-label="Default select example"
                >
                  <option>เลือกเดือน</option>
                  <option value="1">2561</option>
                  <option value="2">2562</option>
                  <option value="3">2563</option>
                </Form.Select>
              </div>
            ) : this.state.value === "day" ? (
              <div style={{ justifyContent: 'end', display: "flex" }}>
                <Form.Select
                  className="select-container"
                  aria-label="Default select example"
                >
                  <option>เลือกวัน</option>
                  <option value="1">2561</option>
                  <option value="2">2562</option>
                  <option value="3">2563</option>
                </Form.Select>
              </div>
            ) : null}
          </Col>
        </Row>
        <Row style={{ margin: "0px", padding: "0px" }}>
          <Table striped bordered hover>
            <thead>
              <tr>
                <th>รหัสสมาชิก</th>
                <th>ชื่อ</th>
                <th>นามสกุล</th>
                <th>ชื่อผู้ใช้</th>
                <th>รหัสชวนเพื่อน</th>
                <th>ตรวจสอบ</th>
                <th>ลบ</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>1</td>
                <td>Mark</td>
                <td>Otto</td>
                <td>@mdo</td>
                <td>1</td>
                <td>
                  <Button
                    onClick={() => {
                      window.location.href = "/admin/member/edit";
                    }}
                    className="button-custom-order-report"
                    variant="warning"
                  >
                    ตรวจสอบ
                  </Button>
                </td>
                <td>
                  <Button
                    className="button-custom-order-report"
                    variant="danger"
                  >
                    ลบ
                  </Button>
                </td>
              </tr>
              <tr>
                <td>2</td>
                <td>Jacob</td>
                <td>Thornton</td>
                <td>@fat</td>
                <td>1</td>
                <td>
                  <Button
                    className="button-custom-order-report"
                    variant="warning"
                  >
                    ตรวจสอบ
                  </Button>
                </td>
                <td>
                  <Button
                    className="button-custom-order-report"
                    variant="danger"
                  >
                    ลบ
                  </Button>
                </td>
              </tr>
              <tr>
                <td>3</td>
                <td colSpan="2">Larry the Bird</td>
                <td>@twitter</td>
                <td>1</td>
                <td>
                  <Button
                    className="button-custom-order-report"
                    variant="warning"
                  >
                    ตรวจสอบ
                  </Button>
                </td>
                <td>
                  <Button
                    className="button-custom-order-report"
                    variant="danger"
                  >
                    ลบ
                  </Button>
                </td>
              </tr>
            </tbody>
          </Table>
        </Row>
      </div>


      // <div className="container-order-report">
      //   <section className="first-main-order-report">
      //     <Row>
      //       <Col>
      //         <div>
      //           <h1>รายงานการซื้อ-ขายสินค้า</h1>
      //         </div>
      //       </Col>
      //       <Col>
      //         <div>
      //           <Form.Select
      //             className="select-container"
      //             aria-label="Default select example"
      //             value={this.state.value}
      //             onChange={this.handleChange}
      //           >
      //             <option>เลือกช่วงเวลา</option>
      //             <option value="day">ภายในวัน</option>
      //             <option value="month">ภายในเดือน</option>
      //             <option value="year">ภายในปี</option>
      //           </Form.Select>
      //         </div>
      //       </Col>
      //       <Col>
      //         {this.state.value === "year" ? (
      //           <div>
      //             <Form.Select
      //               className="select-container"
      //               aria-label="Default select example"
      //             >
      //               <option>เลือกปี</option>
      //               <option value="1">2561</option>
      //               <option value="2">2562</option>
      //               <option value="3">2563</option>
      //             </Form.Select>
      //           </div>
      //         ) : this.state.value === "month" ? (
      //           <div>
      //             <Form.Select
      //               className="select-container"
      //               aria-label="Default select example"
      //             >
      //               <option>เลือกเดือน</option>
      //               <option value="1">2561</option>
      //               <option value="2">2562</option>
      //               <option value="3">2563</option>
      //             </Form.Select>
      //           </div>
      //         ) : this.state.value === "day" ? (
      //           <div>
      //             <Form.Select
      //               className="select-container"
      //               aria-label="Default select example"
      //             >
      //               <option>เลือกวัน</option>
      //               <option value="1">2561</option>
      //               <option value="2">2562</option>
      //               <option value="3">2563</option>
      //             </Form.Select>
      //           </div>
      //         ) : null}
      //       </Col>
      //     </Row>
      //   </section>
      //   <section className="display-order-report">
      //     <div>
      //       <Table
      //         striped
      //         bordered
      //         hover
      //         className="table-container-order-report"
      //       >
      //         <thead>
      //           <tr>
      //             <th>รหัสสมาชิก</th>
      //             <th>ชื่อ</th>
      //             <th>นามสกุล</th>
      //             <th>ชื่อผู้ใช้</th>
      //             <th>รหัสชวนเพื่อน</th>
      //             <th>ตรวจสอบ</th>
      //             <th>ลบ</th>
      //           </tr>
      //         </thead>
      //         <tbody>
      //           <tr>
      //             <td>1</td>
      //             <td>Mark</td>
      //             <td>Otto</td>
      //             <td>@mdo</td>
      //             <td>1</td>
      //             <td>
      //               <Button
      //                 onClick={() => {
      //                   window.location.href = "/admin/member/edit";
      //                 }}
      //                 className="button-custom-order-report"
      //                 variant="warning"
      //               >
      //                 ตรวจสอบ
      //               </Button>
      //             </td>
      //             <td>
      //               <Button
      //                 className="button-custom-order-report"
      //                 variant="danger"
      //               >
      //                 ลบ
      //               </Button>
      //             </td>
      //           </tr>
      //           <tr>
      //             <td>2</td>
      //             <td>Jacob</td>
      //             <td>Thornton</td>
      //             <td>@fat</td>
      //             <td>1</td>
      //             <td>
      //               <Button
      //                 className="button-custom-order-report"
      //                 variant="warning"
      //               >
      //                 ตรวจสอบ
      //               </Button>
      //             </td>
      //             <td>
      //               <Button
      //                 className="button-custom-order-report"
      //                 variant="danger"
      //               >
      //                 ลบ
      //               </Button>
      //             </td>
      //           </tr>
      //           <tr>
      //             <td>3</td>
      //             <td colSpan="2">Larry the Bird</td>
      //             <td>@twitter</td>
      //             <td>1</td>
      //             <td>
      //               <Button
      //                 className="button-custom-order-report"
      //                 variant="warning"
      //               >
      //                 ตรวจสอบ
      //               </Button>
      //             </td>
      //             <td>
      //               <Button
      //                 className="button-custom-order-report"
      //                 variant="danger"
      //               >
      //                 ลบ
      //               </Button>
      //             </td>
      //           </tr>
      //         </tbody>
      //       </Table>
      //     </div>
      //   </section>
      // </div>
    );
  }
}
