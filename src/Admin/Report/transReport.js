import React, { Component } from "react";
import { Row, Col, Form, Button, Table } from "react-bootstrap";
import "rsuite/lib/styles/index.less";
import treeOne from "../../assets/images/category/tree1.jpg";
import treeTwo from "../../assets/images/category/tree2.jpg";
import treeThree from "../../assets/images/category/tree3.jpg";
import treeFour from "../../assets/images/category/tree4.jpg";
import treeFive from "../../assets/images/category/tree5.jpg";
import treeSix from "../../assets/images/category/tree6.jpg";
import treeSale from "../../assets/images/category/tree7.jpg";
import "./report.scss";

export default class TransReport extends Component {
  render() {
    return (

      <div style={{ margin: "0 10%" }}>
        <Row style={{ margin: "0px", padding: "20px 0", alignItems: "center", display: "flex" }}>
          <Col style={{ padding: "0px" }}>
            <h1>รายงานการส่งสินค้า</h1>
          </Col>
        </Row>
        <Row style={{ margin: "0px", padding: "0px" }}>
          <Table striped bordered hover>
            <thead>
              <tr>
                <th>รหัสสมาชิก</th>
                <th>ชื่อ</th>
                <th>นามสกุล</th>
                <th>ชื่อผู้ใช้</th>
                <th>รหัสชวนเพื่อน</th>
                <th>แก้ไข</th>
                <th>ลบ</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>1</td>
                <td>Mark</td>
                <td>Otto</td>
                <td>@mdo</td>
                <td>1</td>
                <td>
                  <Button
                    onClick={() => {
                      window.location.href = "/admin/member/edit";
                    }}
                    className="button-custom-member"
                    variant="warning"
                  >
                    แก้ไข
                  </Button>
                </td>
                <td>
                  <Button className="button-custom-member" variant="danger">
                    ลบ
                  </Button>
                </td>
              </tr>
              <tr>
                <td>2</td>
                <td>Jacob</td>
                <td>Thornton</td>
                <td>@fat</td>
                <td>1</td>
                <td>
                  <Button className="button-custom-member" variant="warning">
                    แก้ไข
                  </Button>
                </td>
                <td>
                  <Button className="button-custom-member" variant="danger">
                    ลบ
                  </Button>
                </td>
              </tr>
              <tr>
                <td>3</td>
                <td colSpan="2">Larry the Bird</td>
                <td>@twitter</td>
                <td>1</td>
                <td>
                  <Button className="button-custom-member" variant="warning">
                    แก้ไข
                  </Button>
                </td>
                <td>
                  <Button className="button-custom-member" variant="danger">
                    ลบ
                  </Button>
                </td>
              </tr>
            </tbody>
          </Table>
        </Row>
      </div>
      // <div className="container-member">
      //   <section className="first-main-member">
      //     <div>
      //       <h1>รายงานการส่งสินค้า</h1>
      //     </div>
      //   </section>
      //   <section className="display-member">
      //     <div>
      //       <Table striped bordered hover className="table-container-member">
      //         <thead>
      //           <tr>
      //             <th>รหัสสมาชิก</th>
      //             <th>ชื่อ</th>
      //             <th>นามสกุล</th>
      //             <th>ชื่อผู้ใช้</th>
      //             <th>รหัสชวนเพื่อน</th>
      //             <th>แก้ไข</th>
      //             <th>ลบ</th>
      //           </tr>
      //         </thead>
      //         <tbody>
      //           <tr>
      //             <td>1</td>
      //             <td>Mark</td>
      //             <td>Otto</td>
      //             <td>@mdo</td>
      //             <td>1</td>
      //             <td>
      //               <Button
      //                 onClick={() => {
      //                   window.location.href = "/admin/member/edit";
      //                 }}
      //                 className="button-custom-member"
      //                 variant="warning"
      //               >
      //                 แก้ไข
      //               </Button>
      //             </td>
      //             <td>
      //               <Button className="button-custom-member" variant="danger">
      //                 ลบ
      //               </Button>
      //             </td>
      //           </tr>
      //           <tr>
      //             <td>2</td>
      //             <td>Jacob</td>
      //             <td>Thornton</td>
      //             <td>@fat</td>
      //             <td>1</td>
      //             <td>
      //               <Button className="button-custom-member" variant="warning">
      //                 แก้ไข
      //               </Button>
      //             </td>
      //             <td>
      //               <Button className="button-custom-member" variant="danger">
      //                 ลบ
      //               </Button>
      //             </td>
      //           </tr>
      //           <tr>
      //             <td>3</td>
      //             <td colSpan="2">Larry the Bird</td>
      //             <td>@twitter</td>
      //             <td>1</td>
      //             <td>
      //               <Button className="button-custom-member" variant="warning">
      //                 แก้ไข
      //               </Button>
      //             </td>
      //             <td>
      //               <Button className="button-custom-member" variant="danger">
      //                 ลบ
      //               </Button>
      //             </td>
      //           </tr>
      //         </tbody>
      //       </Table>
      //     </div>
      //   </section>
      // </div>
    );
  }
}
