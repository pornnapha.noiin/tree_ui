import HomeAdmin from './Home/homeAdmin'

import ManageMember from './ManageMember/manageMember'
import EditMember from './ManageMember/EditMember/editMember'

import ManageOrder from './ManageOrder/manageOrder'
import EditOrder from './ManageOrder/EditOrder/editOrder'

import ManageProduct from './ManageProduct/manageProduct'
import CreateCategory from './ManageProduct/CategoryProduct/createCategory'
import SaveCategory from './ManageProduct/CategoryProduct/saveCategory'
import EditProduct from './ManageProduct/EditProduct/editProduct'
import Manage from './ManageProduct/Manage/manage'
import ProductHome from './ManageProduct/ProductHome/productHome'
import SaveProduct from './ManageProduct/SaveProduct/saveProduct'
import TabProduct from './ManageProduct/Tab/tabProduct'

import OrderReport from './Report/orderReport'
import ReceiveReport from './Report/receiveReport'
import SaleReport from './Report/saleReport'
import TransReport from './Report/transReport'

export {
    HomeAdmin,
    ManageMember,
    EditMember,
    ManageOrder,
    EditOrder,
    ManageProduct,
    CreateCategory,
    SaveCategory,
    EditProduct,
    Manage,
    ProductHome,
    SaveProduct,
    TabProduct,
    OrderReport,
    ReceiveReport,
    SaleReport,
    TransReport
}