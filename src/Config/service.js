import Axios from 'axios'
import { API_URL } from './url_config'

export const POST = (path, formData) => {
    return new Promise((resolve, reject) => {
        Axios.post(API_URL + path, formData && formData).then(
            function (res) {
                resolve(res.data)
            }
        ).catch((err) => reject(err))
    })
}
