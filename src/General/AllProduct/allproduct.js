import React, { Component } from "react";
import { Row, Col, Form, Button, Table } from "react-bootstrap";
import "rsuite/lib/styles/index.less";
import treeOne from "../../assets/images/category/tree1.jpg";
import treeTwo from "../../assets/images/category/tree2.jpg";
import treeThree from "../../assets/images/category/tree3.jpg";
import treeFour from "../../assets/images/category/tree4.jpg";
import treeFive from "../../assets/images/category/tree5.jpg";
import treeSix from "../../assets/images/category/tree6.jpg";
import treeNine from "../../assets/images/category/tree9.jpg";
import treeTen from "../../assets/images/category/tree10.jpg";
import treeEleven from "../../assets/images/category/tree11.jpg";
import "./allproduct.scss";

export default class AllProduct extends Component {
  constructor(props) {
    super(props);
  }

  showDetail = (e) => {
    e.preventDefault();
    window.location.href = "/detail";
  };

  render() {
    return (
      <div className="container-main-all">
        <section className="first-main-all">
          <div>
            <h1>สินค้าทั้งหมด</h1>
          </div>
        </section>
        <section className="display-all">
          <div>
            <h1>หมวดหมู่</h1>
          </div>
          <Row>
            <Col>
              <div class="box1">
                <img
                  alt=""
                  src={treeEleven}
                  style={{
                    borderRadius: "5px",
                    width: "100%",
                  }}
                />
              </div>
            </Col>
            <Col>
              <div class="box2">
                <img
                  alt=""
                  src={treeOne}
                  style={{
                    borderRadius: "5px",
                    width: "100%",
                  }}
                />
              </div>
            </Col>
            <Col>
              <div class="box3">
                <img
                  alt=""
                  src={treeTwo}
                  style={{
                    borderRadius: "5px",
                    width: "100%",
                  }}
                />
              </div>
            </Col>
            <Col>
              <div class="box4">
                <img
                  alt=""
                  src={treeThree}
                  style={{
                    borderRadius: "5px",
                    width: "100%",
                  }}
                />
              </div>
            </Col>
            <Col>
              <div class="box5">
                <img
                  alt=""
                  src={treeTen}
                  style={{
                    borderRadius: "5px",
                    width: "100%",
                  }}
                />
              </div>
            </Col>
            <Col>
              <div class="box6">
                <img
                  alt=""
                  src={treeFive}
                  style={{
                    borderRadius: "5px",
                    width: "100%",
                  }}
                />
              </div>
            </Col>
          </Row>
        </section>
        <section className="display-all">
          <div>
            <h1>สินค้าทั้งหมด</h1>
          </div>
          <div>
            <Table striped bordered hover>
              <tbody>
                <tr>
                  <td className="table-border" onClick={this.showDetail}>
                    <div>
                      <img alt="" src={treeFour} className="img-width" />
                    </div>
                    <div>
                      <Row>
                        <div>ต้นจำปา</div>
                      </Row>
                      <Row>
                        <div>600 บาท</div>
                      </Row>
                    </div>
                  </td>
                  <td className="table-border">
                    <img alt="" src={treeFour} className="img-width" />
                  </td>
                  <td className="table-border">
                    <img alt="" src={treeFour} className="img-width" />
                  </td>
                  <td className="table-border">
                    <img alt="" src={treeFour} className="img-width" />
                  </td>
                </tr>
              </tbody>
            </Table>
          </div>
        </section>
      </div>
    );
  }
}
