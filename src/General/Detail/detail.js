/* eslint-disable react/no-unescaped-entities */

import React, { Component } from "react";
import Bar from "../../layout/Navbar/navbar";
import Footer from "../../layout/Footer/footer";
import "../../scss/index.scss";
import "../../scss/_main.scss";
import "../../scss/bulma.scss";
import "../../scss/_qamain.scss";
import "../../scss/_media.scss";
import "../../scss/_main.scss";
import "../../scss/qa_style.scss";
import "../../scss/_qamedia.scss";
import "./detail.scss";
import { Row, Col, Form, Button, Table } from "react-bootstrap";
import treeFour from "../../assets/images/category/tree4.jpg";
import AllProduct from "../AllProduct/allproduct";

class Detail extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <div>
        <Bar />
        <section>
          <div className="container">
            <div className="columns is-centered">
              <Table striped bordered hover className="first-detail">
                <tbody>
                  <tr>
                    <td
                      className="table-border-detail"
                      onClick={this.showDetail}
                    >
                      <div>
                        <img alt="" src={treeFour} className="img-width" />
                      </div>
                      <div>
                        <Row>
                          <div>ต้นจำปา</div>
                        </Row>
                        <Row>
                          <div>600 บาท</div>
                        </Row>
                      </div>
                    </td>
                    <td className="table-border-detail">
                      <img alt="" src={treeFour} className="img-width" />
                    </td>
                  </tr>
                </tbody>
              </Table>
            </div>
          </div>
        </section>
        <section>
          <div className="container">
            <div className="columns is-centered">รายละเอียดสินค้า</div>
            <Table striped bordered hover className="first-detail">
              <tbody>
                <tr>
                  <td className="table-border-detail" onClick={this.showDetail}>
                    <div>ข้อมูล</div>
                  </td>
                  <td className="table-border-detail">
                    ดอกสีชมพูสวย ขยันออกดอกมากก และอยู่ได้นานด้วย
                  </td>
                </tr>
                <tr>
                  <td className="table-border-detail" onClick={this.showDetail}>
                    <div>ข้อมูล</div>
                  </td>
                  <td className="table-border-detail">
                    ดอกสีชมพูสวย ขยันออกดอกมากก และอยู่ได้นานด้วย
                  </td>
                </tr>
              </tbody>
            </Table>
          </div>
        </section>
        {/* ทำเป็น slick slide */}
        <section>
          <div className="container">
            <div className="columns is-centered">รายละเอียดสินค้า</div>
            <Table striped bordered hover className="first-detail">
              <tbody>
                <tr>
                  <td className="table-border-detail" onClick={this.showDetail}>
                    <div>
                      <img alt="" src={treeFour} className="img-width" />
                    </div>
                    <div>
                      <Row>
                        <div>ต้นจำปา</div>
                      </Row>
                      <Row>
                        <div>600 บาท</div>
                      </Row>
                    </div>
                  </td>
                  <td className="table-border-detail">
                    <img alt="" src={treeFour} className="img-width" />
                  </td>
                </tr>
              </tbody>
            </Table>
          </div>
        </section>
        <section>
          <div className="container">
            <div className="columns is-centered">วิธีการสั่งซื้อสินค้า</div>
            <div>
              <Button>ไปที่หน้าแจ้งชำระเงิน</Button>
            </div>
          </div>
        </section>
        <section>
          <div className="container">
            <div className="columns is-centered">รีวิว</div>
          </div>
        </section>
        <section>
          <div className="container">
            <div>
              <AllProduct />
            </div>
          </div>
        </section>
        <Footer />
      </div>
    );
  }
}

export default Detail;
