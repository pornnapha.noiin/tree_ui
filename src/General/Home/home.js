/* eslint-disable react/no-unescaped-entities */

import React, { Component } from "react";
import Bar from "../../layout/Navbar/navbar";
import Footer from "../../layout/Footer/footer";
import Main from "../Main/main";
import "../../scss/index.scss";
import "../../scss/_main.scss";
import "../../scss/bulma.scss";
import "../../scss/_qamain.scss";
import "../../scss/_media.scss";
import "../../scss/_main.scss";
import "../../scss/qa_style.scss";
import "../../scss/_qamedia.scss";
import Howto from "../Howto/howto";
import Inform from "../Inform/Inform";
import AllProduct from "../AllProduct/allproduct";

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      listMenu: "",
      checkActive: false,
      value: "main_pc",
      windowWidth: window.innerWidth,
      userName: localStorage.getItem("userName"),
      passWord: localStorage.getItem("passWord"),
      userType: localStorage.getItem("userType"),
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleClick = this.handleClick.bind(this);
    console.log("userType", this.state.userType);
    console.log("userName", this.state.userName);
    console.log("passWord", this.state.passWord);
  }
  handleResize = () => {
    this.setState({
      windowWidth: window.innerWidth,
    });
  };

  showListMenu(listMenuName) {
    if (
      listMenuName.name !== this.state.listMenu &&
      listMenuName.active === true
    ) {
      this.setState({
        listMenu: listMenuName.name,
        checkActive: true,
      });
    }
  }

  handleChange(event) {
    this.setState({
      value: event.target.value,
    });
  }

  handleClick(event) {
    this.setState({
      value: event.target.id,
    });
    console.log("****value****", this.state.value);
  }

  render() {
    const { windowWidth } = this.state;
    return (
      <div id="container">
        <Bar />
        <section id="sec__sub--qa" className="section sec__qa">
          <div className="container">
            <div className="columns is-centered">
              <div className="column is-12-desktop is-full-tablet">
                <div className="ele__txt--title">
                  <h1>Online Tree Sale</h1>
                </div>
                <div className="ele__btn--pc">
                  <a
                    onClick={this.handleClick}
                    className={this.state.value === "main_pc" ? "active" : ""}
                    id="main_pc"
                  >
                    หน้าแรก
                  </a>
                  <a
                    onClick={this.handleClick}
                    className={this.state.value === "store_pc" ? "active" : ""}
                    id="store_pc"
                  >
                    สินค้าทั้งหมด
                  </a>
                  <a
                    onClick={this.handleClick}
                    className={this.state.value === "howto_pc" ? "active" : ""}
                    id="howto_pc"
                  >
                    วิธีการสั่งซื้อสินค้า
                  </a>
                  <a
                    onClick={this.handleClick}
                    className={this.state.value === "inform_pc" ? "active" : ""}
                    id="inform_pc"
                  >
                    แจ้งชำระเงิน
                  </a>
                </div>
                <div className="ele__btn--mb" id="select__group">
                  <div className="select">
                    <select
                      value={this.state.value}
                      onChange={this.handleChange}
                    >
                      <option value="select_main">หน้าแรก</option>
                      <option value="select_store">สินค้าทั้งหมด</option>
                      <option value="select_howto">
                        วิธีการสั่งซื้อสินค้า
                      </option>
                      <option value="select_inform">แจ้งชำระเงิน</option>
                    </select>
                  </div>
                </div>
                <hr className="ele__divider" />
              </div>
            </div>
          </div>
        </section>

        <section
          id="sec__faq"
          className={
            this.state.value === "main_pc" || this.state.value === "select_main"
              ? "section sec__col"
              : "section sec__col hiddle-content"
          }
        >
          <div className="container">
            <div className="columns is-centered">
              {this.state.value === "main_pc" ||
              this.state.value === "select_main" ? (
                <div>
                  <Main />
                </div>
              ) : null}
            </div>
          </div>
        </section>

        <section
          id="sec__howto"
          className={
            this.state.value === "store_pc" ||
            this.state.value === "select_store"
              ? "section sec__col"
              : "section sec__col hiddle-content"
          }
        >
          <div className="container">
            <div className="columns is-centered">
              {this.state.value === "store_pc" ||
              this.state.value === "select_store" ? (
                // <div style={{ backgroundColor: "red" }}>hello</div>
                <div>
                  <AllProduct />
                </div>
              ) : null}
            </div>
          </div>
        </section>

        <section
          id="sec__problem"
          className={
            this.state.value === "howto_pc" ||
            this.state.value === "select_howto"
              ? "section sec__col"
              : "section sec__col hiddle-content"
          }
        >
          <div className="container">
            <div className="columns is-centered">
              {this.state.value === "howto_pc" ||
              this.state.value === "select_howto" ? (
                <div>
                  <Howto />
                </div>
              ) : null}
            </div>
          </div>
        </section>

        <section
          id="sec__detail"
          className={
            this.state.value === "inform_pc" ||
            this.state.value === "select_inform"
              ? "section sec__col"
              : "section sec__col hiddle-content"
          }
        >
          <div className="container">
            <div className="columns is-centered">
              {this.state.value === "inform_pc" ||
              this.state.value === "select_inform" ? (
                <div>
                  <Inform />
                </div>
              ) : null}
            </div>
          </div>
        </section>

        <Footer />
      </div>
    );
  }
}

export default Home;
