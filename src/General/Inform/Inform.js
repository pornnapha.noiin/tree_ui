import React, { Component } from "react";
import { Row, Col, Form, Button } from "react-bootstrap";
import "rsuite/lib/styles/index.less";
import treeOne from "../../assets/images/category/tree1.jpg";
import treeTwo from "../../assets/images/category/tree2.jpg";
import treeThree from "../../assets/images/category/tree3.jpg";
import treeFour from "../../assets/images/category/tree4.jpg";
import treeFive from "../../assets/images/category/tree5.jpg";
import treeSix from "../../assets/images/category/tree6.jpg";
import treeSale from "../../assets/images/category/tree7.jpg";
import "./Inform.scss";

export default class Inform extends Component {
  constructor(props) {
    super(props);
    this.state = {
      imagePreviewUrlAns: null,
    };
    this.fileChangedHandlerAns = this.fileChangedHandlerAns.bind(this);
  }

  fileChangedHandlerAns = (event) => {
    this.setState({
      // selectedFileAns: event.target.files[0]
      labPicAns: event.target.files[0],
      fileAns: event.target.files[0],
    });

    let reader = new FileReader();

    reader.onloadend = () => {
      this.setState({
        imagePreviewUrlAns: reader.result,
      });
    };

    reader.readAsDataURL(event.target.files[0]);
  };

  render() {
    let $imagePreviewAns = (
      <div className="previewText image-container">
        {/* Please select an Image for Preview */}
        เลือกรูปภาพตัวอย่างผลลัพธ์แบบฝึกหัด
      </div>
    );
    if (this.state.imagePreviewUrlAns) {
      $imagePreviewAns = (
        <div className="image-container">
          <img src={this.state.imagePreviewUrlAns} alt="icon" width="200" />
        </div>
      );
    }
    return (
      <div className="container-main-howto">
        <section className="first-main-howto">
          <div>
            <h1>แจ้งชำระเงิน</h1>
          </div>
        </section>
        <section className="display-account-bank">
          <div>
            <Row>
              <Col>1</Col>
              <Col>2</Col>
              <Col>3</Col>
              <Col>4</Col>
            </Row>
          </div>
          <div>
            <Row>
              <Col>1</Col>
              <Col>2</Col>
              <Col>3</Col>
              <Col>4</Col>
            </Row>
          </div>
        </section>
        <section className="display-evidence-transfer">
          <Form
            // onSubmit={this.onSubmit}
            style={{ paddingTop: "10px" }}
            // noValidate
          >
            <Row className="margin-form">
              <Col>
                <Form.Group
                  // onChange={this.onChange}
                  as={Col}
                  controlId="formBasicName"
                >
                  <Form.Label className="label-text">
                    เวลาที่ชำระเงิน
                  </Form.Label>
                  <Form.Control
                    className="padding-form"
                    size="sm"
                    type="text"
                    name="userFirstName"
                    placeholder="เวลาที่ชำระเงิน"
                    // onChange={this.handlenameChange}
                  />
                  {/* <div className="invalid-feedback d-block">
                      {this.state.errorname}
                    </div> */}
                </Form.Group>
              </Col>
              <Col>
                <Form.Group
                  // onChange={this.onChange}
                  as={Col}
                  controlId="exampleForm.ControlSelect1"
                >
                  <Form.Label className="label-text">
                    เวลา(โดยประมาณ)
                  </Form.Label>
                  <Form.Control
                    className="padding-form"
                    size="sm"
                    name="prefixId"
                    as="select"
                  >
                    <option>ชั่วโมง</option>
                    <option>นาย</option>
                    <option>นางสาว</option>
                    <option>นาง</option>
                  </Form.Control>
                </Form.Group>
              </Col>
              <Col>
                <Form.Group>
                  <Form.Label className="label-text">&nbsp;</Form.Label>
                  <Form.Control
                    className="padding-form"
                    size="sm"
                    name="prefixId"
                    as="select"
                  >
                    <option>นาที</option>
                    <option>นาย</option>
                    <option>นางสาว</option>
                    <option>นาง</option>
                  </Form.Control>
                </Form.Group>
              </Col>
            </Row>
            <Row className="margin-form">
              <Col>
                <Form.Group
                  // onChange={this.onChange}
                  as={Col}
                  // controlId="formBasicSurname"
                >
                  <Form.Label className="label-text">จำนวนเงิน</Form.Label>
                  <Form.Control
                    className="padding-form"
                    size="sm"
                    type="text"
                    name="tel"
                    placeholder="จำนวนเงิน"
                    // onChange={this.handlesurnameChange}
                  />
                  {/* <div className="invalid-feedback d-block">
                      {this.state.errorsurname}
                    </div> */}
                </Form.Group>
              </Col>
              <Col>
                <Form.Group
                  // onChange={this.onChange}
                  as={Col}
                  // controlId="formBasicSurname"
                >
                  <Form.Label className="label-text">
                    เลขที่ใบสั่งซื้อ
                  </Form.Label>
                  <Form.Control
                    className="padding-form"
                    size="sm"
                    type="text"
                    name="email"
                    placeholder="เลขที่ใบสั่งซื้อ"
                    // onChange={this.handlesurnameChange}
                  />
                  {/* <div className="invalid-feedback d-block">
                      {this.state.errorsurname}
                    </div> */}
                </Form.Group>
              </Col>
            </Row>
          </Form>
        </section>
        <section className="display-description-informer">
          <Form
            // onSubmit={this.onSubmit}
            style={{ paddingTop: "10px" }}
            // noValidate
          >
            <Row>
              <Form.Group
                className="margin-form-last"
                // onChange={this.onChange}
                as={Col}
                // controlId="formBasicSurname"
              >
                <Form.Label className="label-text">
                  อัพโหลดรูปภาพ(หลักฐานการโอนเงิน)
                </Form.Label>
                <div
                  style={{ padding: "5px 0 10px 0" }}
                  className="center-container"
                  className="form-group"
                >
                  <div>
                    <form method="post" action="#" id="#">
                      <div className="form-group files">
                        <input
                          type="file"
                          name="labPicAns"
                          className="form-control"
                          onChange={this.fileChangedHandlerAns}
                        />
                        {$imagePreviewAns}
                      </div>
                      {/* <ToastContainer /> */}
                    </form>
                  </div>
                </div>
              </Form.Group>
            </Row>
            <Row className="margin-form">
              <Form.Label className="label-text">ชื่อผู้แจ้ง</Form.Label>
              <Col>
                <Form.Group
                  // onChange={this.onChange}
                  as={Col}
                  // controlId="formBasicSurname"
                >
                  <Form.Control
                    className="padding-form"
                    size="sm"
                    type="text"
                    name="tel"
                    placeholder="ชื่อจริง"
                    // onChange={this.handlesurnameChange}
                  />
                </Form.Group>
              </Col>
              <Col>
                <Form.Group
                  // onChange={this.onChange}
                  as={Col}
                  // controlId="formBasicSurname"
                >
                  <Form.Control
                    className="padding-form"
                    size="sm"
                    type="text"
                    name="tel"
                    placeholder="นามสกุล"
                    // onChange={this.handlesurnameChange}
                  />
                  {/* <div className="invalid-feedback d-block">
                      {this.state.errorsurname}
                    </div> */}
                </Form.Group>
              </Col>
              <Col>
                <Form.Group
                  // onChange={this.onChange}
                  as={Col}
                  // controlId="formBasicSurname"
                >
                  <Form.Control
                    className="padding-form"
                    size="sm"
                    type="text"
                    name="email"
                    placeholder="เบอร์ติดต่อ"
                    // onChange={this.handlesurnameChange}
                  />
                  {/* <div className="invalid-feedback d-block">
                      {this.state.errorsurname}
                    </div> */}
                </Form.Group>
              </Col>
            </Row>
            <Row>
              <Col>
                <Form.Group>
                  <Form.Label className="label-text">
                    รายละเอียดเพิ่มเติม
                  </Form.Label>
                  <Form.Control
                    className="padding-form"
                    size="sm"
                    type="text"
                    name="email"
                    placeholder="รายละเอียดเพิ่มเติม"
                    // onChange={this.handlesurnameChange}
                  />
                </Form.Group>
              </Col>
            </Row>
          </Form>
        </section>
        <section className="display-inform-submit">
          <div className="card-container">
            {/* {this.state.statusDateTime === 1 ? ( */}
            <Button
              // to="/login"
              // onClick={() => {
              //   window.location.href = "/login";
              // }}
              size="sm"
              variant="danger"
              style={{ marginTop: "5px", width: "7rem" }}
            >
              ยกเลิก
            </Button>
            {/* ) : null} */}
            &nbsp;
            <Button
              size="sm"
              variant="info"
              type="submit"
              style={{ marginTop: "5px", width: "7rem" }}
            >
              แจ้งชำระเงิน
            </Button>
          </div>
        </section>
      </div>
    );
  }
}
