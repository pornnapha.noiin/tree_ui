import React, { Component } from "react";
import treeOne from "../../assets/images/category/tree1.jpg";
import treeTwo from "../../assets/images/category/tree2.jpg";
import treeThree from "../../assets/images/category/tree3.jpg";
import treeFour from "../../assets/images/category/tree4.jpg";
import treeFive from "../../assets/images/category/tree5.jpg";
import treeSix from "../../assets/images/category/tree6.jpg";
import treeSale from "../../assets/images/category/tree7.jpg";
import "./main.scss";

export default class Main extends Component {
  render() {
    return (
      <div className="container-main">
        <section className="first-main">
          <div style={{ color: "red" }}> hellooooooooo </div>
        </section>
        <section className="display-category-top">
          <div class="box1">
            <img
              alt=""
              src={treeSix}
              style={{
                borderRadius: "5px",
                width: "98%",
                height: "100%",
                objectFit: "fill",
              }}
            />
          </div>
          <div class="box2">
            <img
              alt=""
              src={treeOne}
              style={{
                borderRadius: "5px",
                width: "99%",
              }}
            />
          </div>
          <div class="box3">
            <img
              alt=""
              src={treeTwo}
              style={{
                borderRadius: "5px",
                width: "99%",
              }}
            />
          </div>
        </section>
        <section className="display-category-second">
          <div class="box4">
            <img
              alt=""
              src={treeThree}
              style={{
                borderRadius: "5px",
                width: "100%",
              }}
            />
          </div>
          <div class="box5">
            <img
              alt=""
              src={treeFour}
              style={{
                borderRadius: "5px",
                width: "99%",
              }}
            />
          </div>
        </section>
        <section className="display-category-third">
          <div class="box6">
            <img
              alt=""
              src={treeFive}
              style={{
                borderRadius: "5px",
                width: "100%",
              }}
            />
          </div>
        </section>
        <section className="display-category-sale">
          <div class="box7">
            <img
              alt=""
              src={treeSale}
              style={{
                borderRadius: "5px",
                width: "100%",
              }}
            />
          </div>
        </section>
      </div>
    );
  }
}
