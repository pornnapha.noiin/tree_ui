import React, { Component } from "react";
import Bar from "../layout/Navbar/navbar";
import "bootstrap/dist/css/bootstrap.min.css";
import { Button, Row, Col, Form, Card, Spinner } from "react-bootstrap";
import "./login.scss";
import logoTreeSale from "../assets/images/mytree.jpg";
import alertConfirm, { alert } from "react-alert-confirm";
import { POST } from "Config/service";

// const validEmailRegex = RegExp(
//   /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i
// );
// const validUserNameRegex = RegExp(/^([0-9]{11}(\-[0-9]{1}))?$/i);
// const validUserPasswordRegex = RegExp(
//   /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]).{8,}$/i
// );

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userName: "",
      passWord: "",
      userType: "",
      isLoading: false
    };
    console.log("userType in login: ", this.state.userType);

    // this.handlenameChange = this.handlenameChange.bind(this);
    // this.handlesurnameChange = this.handlesurnameChange.bind(this);
  }

  // handlenameChange = (e) => {
  //   if (e.target.value.match("^[ก-๙]+$")) {
  //     this.setState({
  //       name: e.target.value,
  //       errorname: "",
  //     });
  //   } else {
  //     this.setState({
  //       errorname: "กรอกได้เฉพาะตัวอักษรภาษาไทย",
  //     });
  //   }
  // };

  // handlesurnameChange = (e) => {
  //   if (e.target.value.match("^[ก-๙]+$")) {
  //     this.setState({
  //       name: e.target.value,
  //       errorsurname: "",
  //     });
  //   } else {
  //     this.setState({
  //       errorsurname: "กรอกได้เฉพาะตัวอักษรภาษาไทย",
  //     });
  //   }
  // };

  onChange = (e) => {
    const { name, value } = e.target;

    this.setState({
      [name]: value,
    });
  };

  // handleChange = (event) => {
  //   event.preventDefault();
  //   const { name, value } = event.target;
  //   let errors = this.state.errors;
  //   switch (name) {
  //     case "userSurname":
  //       errors.userSurname = value.length < 1 ? "กรุณากรอกนามสกุล" : "";
  //       break;
  //     case "userName":
  //       errors.userName = validUserNameRegex.test(value)
  //         ? ""
  //         : "รูปแบบรหัสนักศึกษาไม่ถูกต้อง";
  //       break;

  //     case "email":
  //       errors.email = validEmailRegex.test(value)
  //         ? ""
  //         : "รูปแบบอีเมลไม่ถูกต้อง";
  //       break;
  //     case "userPassword":
  //       errors.userPassword = validUserPasswordRegex.test(value)
  //         ? ""
  //         : "ต้องมี A-Z a-z 0-9 ตัวอักษรพิเศษ และมีอย่างน้อย 8 ตัวอักษร";
  //       break;

  //     case "rePassword":
  //     case "rePassword":
  //       errors.rePassword = this.state.userPassword ? "" : "รหัสผ่านไม่ตรงกัน";

  //       break;

  //     default:
  //       break;
  //   }

  //   this.setState({ errors, [name]: value });
  // };

  onLogin = async (e) => {
    e.preventDefault();

    this.setState({ isLoading: true }) // Loading on

    let data = new FormData();
    data.append("userName", this.state.userName);
    data.append("passWord", this.state.passWord);

    let res = await POST("/users/login", data)
    if (res.status == 1) {
      console.log(res.data.userType);
      if (res.data.userType == 1) {
        this.setState({
          isRegister: true,
          userType: res.data.userType,
        });
        localStorage.setItem("userName", res.data.userName);
        localStorage.setItem("passWord", res.data.passWord);
        localStorage.setItem("userType", res.data.userType);
        console.log(res.data.userName);
        console.log(res.data.passWord);
        console.log(res.data.userType);
        setTimeout(() => {
          console.log("you can see me after 2 seconds");
          {
            this.setState({ isLoading: false }) // Reset loading state
            this.redirectRegister();
          }
        }, 2000);
      } else if (res.data.userType == 0) {
        this.setState({
          isAdmin: true,
          userType: res.data.userType,
        });
        localStorage.setItem("userName", res.data.userName);
        // localStorage.setItem("passWord", res.data.passWord);
        localStorage.setItem("userType", res.data.userType);
        console.log(res.data.userName);
        // console.log(res.data.passWord);
        console.log(res.data.userType);
        setTimeout(() => {
          console.log("you can see me after 2 seconds");
          {
            this.setState({ isLoading: false }) // Reset loading state
            this.redirectAdmin();
          }
        }, 2000);
      }
    } else if (res.status == 0) {
      alertConfirm({
        title: "ไม่พบชื่อผู้ใช้ดังกล่าว",
        content: "กรุณาสมัครสมาชิก เพื่อเข้าสู่ระบบ",
        lang: "en",
        okText: "ตกลง",
        cancelText: "กลับ",
        onOk: () => {
          console.log("ตกลง");
        },
        onCancel: () => {
          console.log("กลับ");
        },
      });
      this.setState({ isLoading: false }) // Reset loading state
    }
  };

  redirectRegister() {
    console.log("userType", this.state.userType);
    localStorage.setItem("userName", this.state.userName);
    localStorage.setItem("passWord", this.state.passWord);
    localStorage.setItem("userType", this.state.userType);
    this.props.history.push("/");
  }

  redirectAdmin() {
    console.log("userType", this.state.userType);
    localStorage.setItem("userName", this.state.userName);
    localStorage.setItem("passWord", this.state.passWord);
    localStorage.setItem("userType", this.state.userType);
    this.props.history.push("/admin/home");
  }

  render() {

    let { isLoading } = this.state
    return (
      <div>
        <Bar />
        <div className="body-code-gradient-login">
          <div className="center-container">
            <Card className="card-inside">
              <Card.Body>
                <div className="card-container">
                  <img alt="" src={logoTreeSale} width="45" height="45" />
                </div>
                <div className="card-container">
                  <text className="text-card">เข้าสู่ระบบ</text>
                </div>
                <Form
                  onSubmit={this.onLogin}
                  style={{ paddingTop: "10px" }}
                // noValidate
                >
                  <Row className="margin-form">
                    <Col>
                      <Form.Group
                        onChange={this.onChange}
                        as={Col}
                      // controlId="formBasicSurname"
                      >
                        <Form.Label className="label-text">
                          ชื่อผู้ใช้
                        </Form.Label>
                        <Form.Control
                          className="padding-form"
                          size="sm"
                          type="text"
                          name="userName"
                          placeholder="ชื่อผู้ใช้"
                        // onChange={this.handlesurnameChange}
                        />
                        {/* <div className="invalid-feedback d-block">
                      {this.state.errorsurname}
                    </div> */}
                      </Form.Group>
                    </Col>
                  </Row>
                  <Row>
                    <Col>
                      <Form.Group
                        onChange={this.onChange}
                        as={Col}
                      // controlId="formBasicSurname"
                      >
                        <Form.Label className="label-text">รหัสผ่าน</Form.Label>
                        <Form.Control
                          className="margin-form-last"
                          size="sm"
                          type="text"
                          name="passWord"
                          placeholder="รหัสผ่าน"
                        // onChange={this.handlesurnameChange}
                        />
                        {/* <div className="invalid-feedback d-block">
                      {this.state.errorsurname}
                    </div> */}
                      </Form.Group>
                    </Col>
                  </Row>
                  <div className="card-container">
                    {/* {this.state.statusDateTime === 1 ? ( */}
                    {/* ) : null} */}
                    &nbsp;
                    <Button size="sm" variant="info" type="submit" style={{ marginTop: "5px", width: "7rem" }}>
                      {isLoading ? <><Spinner animation="border" /> เข้าสู่ระบบ</> : 'เข้าสู่ระบบ'}
                    </Button>
                  </div>
                </Form>
              </Card.Body>
            </Card>
          </div>
        </div>
      </div>
    );
  }
}

export default Login;
