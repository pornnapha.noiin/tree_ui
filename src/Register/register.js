import React, { Component } from "react";
import Bar from "../layout/Navbar/navbar";
import "bootstrap/dist/css/bootstrap.min.css";
import {
  Navbar,
  Button,
  Nav,
  ButtonToolbar,
  Container,
  Row,
  Col,
  Form,
  Card,
  DropdownButton,
  Dropdown,
} from "react-bootstrap";
import "./register.scss";
import Axios, { AxiosResponse } from "axios";
import alertConfirm, { alert } from "react-alert-confirm";
import logoTreeSale from "../assets/images/mytree.jpg";
import { API_URL } from "../Config/url_config";
// import { Link } from "react-router-dom";

// const validEmailRegex = RegExp(
//   /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i
// );
// const validUserNameRegex = RegExp(/^([0-9]{11}(\-[0-9]{1}))?$/i);
// const validUserPasswordRegex = RegExp(
//   /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]).{8,}$/i
// );

class Register extends Component {
  constructor(props) {
    super(props);
    this.state = {
      prefix: "",
      firstName: "",
      lastName: "",
      email: "",
      tel: "",
      codeGetFriend: "",
      userName: "",
      passWord: "",
      rePassWord: "",
      userType: 1,
      status: true,
      showSave: "",
      errors: {
        firstName: "",
        lastName: "",
        email: "",
        tel: "",
        codeGetFriend: "",
        userName: "",
        passWord: "",
        rePassWord: "",
      },
      error: {
        errorname: "",
        errorsurname: "",
      },
    };

    // this.handlenameChange = this.handlenameChange.bind(this);
    // this.handlesurnameChange = this.handlesurnameChange.bind(this);
  }

  // handlenameChange = (e) => {
  //   if (e.target.value.match("^[ก-๙]+$")) {
  //     this.setState({
  //       name: e.target.value,
  //       errorname: "",
  //     });
  //   } else {
  //     this.setState({
  //       errorname: "กรอกได้เฉพาะตัวอักษรภาษาไทย",
  //     });
  //   }
  // };

  // handlesurnameChange = (e) => {
  //   if (e.target.value.match("^[ก-๙]+$")) {
  //     this.setState({
  //       name: e.target.value,
  //       errorsurname: "",
  //     });
  //   } else {
  //     this.setState({
  //       errorsurname: "กรอกได้เฉพาะตัวอักษรภาษาไทย",
  //     });
  //   }
  // };

  onChange = (e) => {
    const { name, value } = e.target;

    this.setState({
      [name]: value,
    });

    console.log("name : ", this.state.name);
  };

  // handleChange = (event) => {
  //   event.preventDefault();
  //   const { name, value } = event.target;
  //   let errors = this.state.errors;
  //   switch (name) {
  //     case "userSurname":
  //       errors.userSurname = value.length < 1 ? "กรุณากรอกนามสกุล" : "";
  //       break;
  //     case "userName":
  //       errors.userName = validUserNameRegex.test(value)
  //         ? ""
  //         : "รูปแบบรหัสนักศึกษาไม่ถูกต้อง";
  //       break;

  //     case "email":
  //       errors.email = validEmailRegex.test(value)
  //         ? ""
  //         : "รูปแบบอีเมลไม่ถูกต้อง";
  //       break;
  //     case "userPassword":
  //       errors.userPassword = validUserPasswordRegex.test(value)
  //         ? ""
  //         : "ต้องมี A-Z a-z 0-9 ตัวอักษรพิเศษ และมีอย่างน้อย 8 ตัวอักษร";
  //       break;

  //     case "rePassword":
  //     case "rePassword":
  //       errors.rePassword = this.state.userPassword ? "" : "รหัสผ่านไม่ตรงกัน";

  //       break;

  //     default:
  //       break;
  //   }

  //   this.setState({ errors, [name]: value });
  // };

  onSubmit = (e) => {
    e.preventDefault();
    console.log(
      "this.state : ",
      this.state
      // this.state.prefix,
      // this.state.firstName,
      // this.state.lastName,
      // this.state.email,
      // this.state.tel,
      // this.state.codeGetFriend,
      // this.state.userName,
      // this.state.passWord,
      // this.state.rePassWord
    );
    if (
      this.state.prefix === "" ||
      this.state.firstName === "" ||
      this.state.lastName === "" ||
      this.state.email === "" ||
      this.state.tel === "" ||
      this.state.codeGetFriend === "" ||
      this.state.userName === "" ||
      this.state.passWord === "" ||
      this.state.rePassWord === ""
    ) {
      alertConfirm({
        title: "กรอกข้อมูลไม่ครบ",
        content: 'กรุณากรอกข้อมูลให้ครบถ้วน ก่อนการกด "สมัครสมาชิก"',
        lang: "en",
        okText: "ตกลง",
        cancelText: "ออก",
        onOk: () => {
          console.log("ตกลง");
        },
        onCancel: () => {
          console.log("ออก");
        },
      });
    } else {
      if (this.state.passWord === this.state.rePassWord) {
        var data = new FormData();
        data.append("prefix", this.state.prefix);
        data.append("firstName", this.state.firstName);
        data.append("lastName", this.state.lastName);
        data.append("email", this.state.email);
        data.append("tel", this.state.tel);
        data.append("codeGetFriend", this.state.codeGetFriend);
        data.append("userName", this.state.userName);
        data.append("passWord", this.state.passWord);
        data.append("rePassWord", this.state.rePassWord);
        data.append("status", this.state.status);
        data.append("userType", this.state.userType);
        let url = API_URL + "/users/save";
        Axios.post(url, data).then(
          function (res) {
            console.log(res.data);
            this.setState({
              showSave: res.data.message,
            });
            console.log(this.state.showSave);
            if (this.state.showSave === "save ok") {
              alertConfirm({
                title: "สมัครสมาชิกสำเร็จ",
                content: 'กดปุ่ม "LOGIN" เพื่อล็อกอินเข้าสู่ระบบ',
                lang: "en",
                okText: "LOGIN",
                cancelText: "ออก",
                onOk: () => {
                  console.log("LOGIN");
                  window.location.href = "/login";
                },
                onCancel: () => {
                  console.log("ออก");
                },
              });
            } else if (this.state.showSave === "repeat") {
              alertConfirm({
                title: "สมัครสมาชิกไม่สำเร็จ",
                content: "เนื่องจากมีผู้ใช้นี้แล้ว",
                lang: "en",
                okText: "ตกลง",
                cancelText: "ยกเลิก",
                onOk: () => {
                  console.log("ตกลง");
                },
                onCancel: () => {
                  console.log("ยกเลิก");
                },
              });
            } else {
              alertConfirm({
                title: "สมัครสมาชิกไม่สำเร็จ",
                content: "กรุณาลองใหม่อีกครั้ง",
                lang: "en",
                okText: "ตกลง",
                cancelText: "ยกเลิก",
                onOk: () => {
                  console.log("ตกลง");
                },
                onCancel: () => {
                  console.log("ยกเลิก");
                },
              });
            }
          }.bind(this)
        );
      } else {
        console.log("ok");
        alertConfirm({
          title: "รหัสผ่านไม่ตรงกัน",
          content: "สมัครสมาชิกไม่สำเร็จ เนื่องจากการยืนยันรหัสผ่านไม่ตรงกัน",
          lang: "en",
          okText: "ตกลง",
          cancelText: "ยกเลิก",
          onOk: () => {
            console.log("ตกลง");
          },
          onCancel: () => {
            console.log("ยกเลิก");
          },
        });
      }
    }
  };

  render() {
    return (
      <div>
        <Bar />
        <div className="body-code-gradient-login">
          <div className="center-container">
            <Card className="card-inside">
              <Card.Body>
                <div className="card-container">
                  <img alt="" src={logoTreeSale} width="45" height="45" />
                </div>
                <div className="card-container">
                  <text className="text-card">สมัครสมาชิกเพื่อเข้าสู่ระบบ</text>
                </div>
                <Form
                  onSubmit={this.onSubmit}
                  style={{ paddingTop: "10px" }}
                  // noValidate
                >
                  <Row className="margin-form">
                    <Col>
                      <Form.Group
                        onChange={this.onChange}
                        as={Col}
                        controlId="exampleForm.ControlSelect1"
                      >
                        <Form.Label className="label-text">
                          คำนำหน้าชื่อ
                        </Form.Label>
                        <Form.Control
                          className="padding-form"
                          size="sm"
                          name="prefix"
                          as="select"
                        >
                          <option>เลือก</option>
                          <option>นาย</option>
                          <option>นางสาว</option>
                          <option>นาง</option>
                        </Form.Control>
                      </Form.Group>
                    </Col>
                    <Col>
                      <Form.Group
                        onChange={this.onChange}
                        as={Col}
                        controlId="formBasicName"
                      >
                        <Form.Label className="label-text">ชื่อจริง</Form.Label>
                        <Form.Control
                          className="padding-form"
                          size="sm"
                          type="text"
                          name="firstName"
                          placeholder="ชื่อ"
                          // onChange={this.handlenameChange}
                        />
                        {/* <div className="invalid-feedback d-block">
                      {this.state.errorname}
                    </div> */}
                      </Form.Group>
                    </Col>
                    <Col>
                      <Form.Group
                        onChange={this.onChange}
                        as={Col}
                        controlId="formBasicSurname"
                      >
                        <Form.Label className="label-text">นามสกุล</Form.Label>
                        <Form.Control
                          className="padding-form"
                          size="sm"
                          type="text"
                          name="lastName"
                          placeholder="นามสกุล"
                          // onChange={this.handlesurnameChange}
                        />
                        {/* <div className="invalid-feedback d-block">
                      {this.state.errorsurname}
                    </div> */}
                      </Form.Group>
                    </Col>
                    <Col>
                      <Form.Group
                        onChange={this.onChange}
                        as={Col}
                        controlId="formBasicSurname"
                      >
                        <Form.Label className="label-text">อีเมล</Form.Label>
                        <Form.Control
                          className="padding-form"
                          size="sm"
                          type="text"
                          name="email"
                          placeholder="อีเมล"
                          // onChange={this.handlesurnameChange}
                        />
                        {/* <div className="invalid-feedback d-block">
                      {this.state.errorsurname}
                    </div> */}
                      </Form.Group>
                    </Col>
                  </Row>
                  <Row className="margin-form">
                    <Col>
                      <Form.Group
                        onChange={this.onChange}
                        as={Col}
                        // controlId="formBasicSurname"
                      >
                        <Form.Label className="label-text">เบอร์โทร</Form.Label>
                        <Form.Control
                          className="padding-form"
                          size="sm"
                          type="text"
                          name="tel"
                          placeholder="เบอร์โทร"
                          // onChange={this.handlesurnameChange}
                        />
                        {/* <div className="invalid-feedback d-block">
                      {this.state.errorsurname}
                    </div> */}
                      </Form.Group>
                    </Col>
                    <Col>
                      <Form.Group
                        onChange={this.onChange}
                        as={Col}
                        // controlId="formBasicSurname"
                      >
                        <Form.Label className="label-text">
                          ชื่อผู้ใช้
                        </Form.Label>
                        <Form.Control
                          className="padding-form"
                          size="sm"
                          type="text"
                          name="userName"
                          placeholder="ชื่อผู้ใช้"
                          // onChange={this.handlesurnameChange}
                        />
                        {/* <div className="invalid-feedback d-block">
                      {this.state.errorsurname}
                    </div> */}
                      </Form.Group>
                    </Col>
                    <Col>
                      <Form.Group
                        onChange={this.onChange}
                        as={Col}
                        // controlId="formBasicSurname"
                      >
                        <Form.Label className="label-text">รหัสผ่าน</Form.Label>
                        <Form.Control
                          className="padding-form"
                          size="sm"
                          type="password"
                          name="passWord"
                          placeholder="รหัสผ่าน"
                          // onChange={this.handlesurnameChange}
                        />
                        {/* <div className="invalid-feedback d-block">
                      {this.state.errorsurname}
                    </div> */}
                      </Form.Group>
                    </Col>
                    <Col>
                      <Form.Group
                        onChange={this.onChange}
                        as={Col}
                        controlId="formBasicPasswordConfirm"
                      >
                        <Form.Label className="label-text">
                          ยืนยันรหัสผ่าน
                        </Form.Label>
                        <Form.Control
                          className="padding-form"
                          size="sm"
                          name="rePassWord"
                          type="password"
                          placeholder="ยืนยันรหัสผ่าน"
                          // onChange={this.handleChange}
                          // noValidate
                        />
                        {/* {errors.rePassword.length > 0 && (
                          <span
                            className="error"
                            style={{ color: "red", fontSize: "10px" }}
                          >
                            {errors.rePassword}
                          </span>
                        )} */}
                      </Form.Group>
                    </Col>
                  </Row>
                  <Form.Group
                    className="margin-form-last"
                    onChange={this.onChange}
                    as={Col}
                    // controlId="formBasicSurname"
                  >
                    <Form.Label className="label-text">รหัสส่วนลด</Form.Label>
                    <Form.Control
                      className="padding-form"
                      size="sm"
                      type="text"
                      name="codeGetFriend"
                      placeholder="รหัสส่วนลด"
                      // onChange={this.handlesurnameChange}
                    />
                    {/* <div className="invalid-feedback d-block">
                      {this.state.errorsurname}
                    </div> */}
                  </Form.Group>
                  <div className="card-container">
                    {/* {this.state.statusDateTime === 1 ? ( */}
                    <Button
                      // to="/login"
                      // onClick={() => {
                      //   window.location.href = "/login";
                      // }}
                      size="sm"
                      variant="danger"
                      style={{ marginTop: "5px", width: "7rem" }}
                    >
                      ยกเลิก
                    </Button>
                    {/* ) : null} */}
                    &nbsp;
                    <Button
                      size="sm"
                      variant="info"
                      type="submit"
                      style={{ marginTop: "5px", width: "7rem" }}
                    >
                      สมัครสมาชิก
                    </Button>
                  </div>
                </Form>
              </Card.Body>
            </Card>
          </div>
        </div>
      </div>
    );
  }
}

export default Register;
