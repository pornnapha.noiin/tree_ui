import React, { Component } from "react";
import Navbar from "react-bootstrap/Navbar";
import "bootstrap/dist/css/bootstrap.min.css";
import { Nav, Container } from "react-bootstrap";
import logoTreeSale from "../../assets/images/mytree.jpg";

export default class Bar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userName: localStorage.getItem("userName"),
      passWord: localStorage.getItem("passWord"),
      userType: localStorage.getItem("userType"),
    };
    this.logout = this.logout.bind(this);
  }

  logout = async () => {
    localStorage.clear();
    window.location.href = "/login";
  };

  render() {
    return (
      <div>
        <>
          <Navbar className="nav-fix" bg="light" variant="light">
            <Container>
              <Navbar.Brand href="/">
                <img
                  alt=""
                  src={logoTreeSale}
                  className="d-inline-block align-top"
                  style={{
                    margin: "0 10px 0 20px",
                    borderRadius: "20px",
                    width: "2.5rem",
                  }}
                />
                Online Tree Sale
              </Navbar.Brand>
              {!this.state.userName ? (
                <Nav className="me-auto">
                  <Nav.Link href="/login">เข้าสู่ระบบ</Nav.Link>
                  <Nav.Link href="/register">สมัครสมาชิก</Nav.Link>
                </Nav>
              ) : (
                <Nav className="me-auto">
                  <Nav.Link onClick={this.logout}>ออกจากระบบ</Nav.Link>
                </Nav>
              )}
            </Container>
          </Navbar>
        </>
      </div>
    );
  }
}
