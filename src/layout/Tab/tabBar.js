import React, { Component } from "react";
import Tab from "react-bootstrap/Tab";
import "bootstrap/dist/css/bootstrap.min.css";
import "./tabNav.scss";
import {
  Nav,
  Container,
  Row,
  Col,
  Sonnet,
  Navbar,
  Dropdown,
} from "react-bootstrap";
import logoTreeSale from "../../assets/images/mytree.jpg";
import ManageMember from "../../Admin/ManageMember/manageMember";
import ManageOrder from "../../Admin/ManageOrder/manageOrder";
import ManageProduct from "../../Admin/ManageProduct/manageProduct";
import DropdownButton from "react-bootstrap/DropdownButton";
import OrderReport from "../../Admin/Report/orderReport";
import SaleReport from "../../Admin/Report/saleReport";
import TransReport from "../../Admin/Report/transReport";
import ReceiveReport from "../../Admin/Report/receiveReport";

export default class TabBar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      listMenu: "",
      checkActive: false,
      value: "product",
      isShow: false,
    };
    this.handleClick = this.handleClick.bind(this);
    // this.handleChange = this.handleChange.bind(this);
  }
  handleClick(event) {
    this.setState({
      value: event.target.id,
    });
    console.log("****value****", this.state.value);
  }
  showListMenu = (listMenuName) => {
    if (listMenuName.active === true) {
      const box = document.getElementById('slide-down');
      box.style.height = "280px"
      this.setState({
        checkActive: true,
      });
    } else {
      const box = document.getElementById('slide-down');
      box.style.height = "0px"
      this.setState({
        checkActive: false,
      });
    }
  }
  render() {
    return (

      <Row style={{ margin: "0" }}>
        <Col sm={2} style={{ padding: "0", height: "90vh", backgroundColor: "rgba(248,249,250,1)" }}>
          <Nav variant="pills" className="flex-column">
            <Nav.Item>
              <Nav.Link onClick={this.handleClick} id="product">
                สินค้า
              </Nav.Link>
            </Nav.Item>
            <Nav.Item>
              <Nav.Link onClick={this.handleClick} id="register">
                สมาชิก
              </Nav.Link>
            </Nav.Item>
            <Nav.Item>
              <Nav.Link onClick={this.handleClick} id="order">
                คำสั่งซื้อ
              </Nav.Link>
            </Nav.Item>
            <Nav.Item>
              <Nav.Link
                onClick={() => {
                  this.showListMenu({
                    active: !this.state.checkActive,
                  });
                }}
              >
                รายงาน
              </Nav.Link>
            </Nav.Item>
            <div id="slide-down">
              {this.state.checkActive ? (
                <Nav.Item>
                  <Nav.Link
                    onClick={this.handleClick}
                    id="orderReport"
                  >
                    รายงานการซื้อ-ขายสินค้า
                  </Nav.Link>
                </Nav.Item>
              ) : null}
              {this.state.checkActive ? (
                <Nav.Item>
                  <Nav.Link
                    onClick={this.handleClick}
                    id="saleReport"
                  >
                    รายงานการขายสินค้า
                  </Nav.Link>
                </Nav.Item>
              ) : null}
              {this.state.checkActive ? (
                <Nav.Item>
                  <Nav.Link
                    onClick={this.handleClick}
                    id="transReport"
                  >
                    รายงานการส่งสินค้า
                  </Nav.Link>
                </Nav.Item>
              ) : null}
              {this.state.checkActive ? (
                <Nav.Item>
                  <Nav.Link
                    onClick={this.handleClick}
                    id="receiveReport"
                  >
                    รายงานการรับสินค้า
                  </Nav.Link>
                </Nav.Item>
              ) : null}
            </div>
          </Nav>
        </Col>
        <Col sm={10} style={{ padding: "0", height: "auto", backgroundColor: "white" }}>
          {this.state.value === "register" ? (
            <div>
              <ManageMember />
            </div>
          ) : this.state.value === "order" ? (
            <div>
              <ManageOrder />
            </div>
          ) : this.state.value === "product" ? (
            <div>
              <ManageProduct />
            </div>
          ) : this.state.value === "orderReport" ? (
            <div>
              <OrderReport />
            </div>
          ) : this.state.value === "saleReport" ? (
            <div>
              <SaleReport />
            </div>
          ) : this.state.value === "transReport" ? (
            <div>
              <TransReport />
            </div>
          ) : this.state.value === "receiveReport" ? (
            <div>
              <ReceiveReport />
            </div>
          ) : null}
        </Col>
      </Row >
      // <div>
      //   <>
      //     <div>
      //       <Row className="container-bar">
      //         <Col>
      //           <div>
      //             <Navbar className="nav-fix-tab" bg="light" variant="light">
      //               <Row style={{ width: "100%" }}>
      //                 <Col sm={3} className="col-sm-3">
      //                   <Nav variant="pills" className="flex-column">
      //                     <Nav.Item>
      //                       <Nav.Link onClick={this.handleClick} id="product">
      //                         สินค้า
      //                       </Nav.Link>
      //                     </Nav.Item>
      //                     <Nav.Item>
      //                       <Nav.Link onClick={this.handleClick} id="register">
      //                         สมาชิก
      //                       </Nav.Link>
      //                     </Nav.Item>
      //                     <Nav.Item>
      //                       <Nav.Link onClick={this.handleClick} id="order">
      //                         คำสั่งซื้อ
      //                       </Nav.Link>
      //                     </Nav.Item>
      //                     <Nav.Item>
      //                       <Nav.Link
      //                         onClick={() => {
      //                           this.showListMenu({
      //                             active: !this.state.checkActive,
      //                           });
      //                         }}
      //                       >
      //                         รายงาน
      //                       </Nav.Link>
      //                     </Nav.Item>
      //                     {this.state.checkActive ? (
      //                       <Nav.Item>
      //                         <Nav.Link
      //                           onClick={this.handleClick}
      //                           id="orderReport"
      //                         >
      //                           รายงานการซื้อ-ขายสินค้า
      //                         </Nav.Link>
      //                       </Nav.Item>
      //                     ) : null}
      //                     {this.state.checkActive ? (
      //                       <Nav.Item>
      //                         <Nav.Link
      //                           onClick={this.handleClick}
      //                           id="saleReport"
      //                         >
      //                           รายงานการขายสินค้า
      //                         </Nav.Link>
      //                       </Nav.Item>
      //                     ) : null}
      //                     {this.state.checkActive ? (
      //                       <Nav.Item>
      //                         <Nav.Link
      //                           onClick={this.handleClick}
      //                           id="transReport"
      //                         >
      //                           รายงานการส่งสินค้า
      //                         </Nav.Link>
      //                       </Nav.Item>
      //                     ) : null}
      //                     {this.state.checkActive ? (
      //                       <Nav.Item>
      //                         <Nav.Link
      //                           onClick={this.handleClick}
      //                           id="receiveReport"
      //                         >
      //                           รายงานการรับสินค้า
      //                         </Nav.Link>
      //                       </Nav.Item>
      //                     ) : null}
      //                   </Nav>
      //                 </Col>
      //               </Row>
      //             </Navbar>
      //           </div>
      //         </Col>
      //         <Col>
      //           {this.state.value === "register" ? (
      //             <div>
      //               <ManageMember />
      //             </div>
      //           ) : this.state.value === "order" ? (
      //             <div>
      //               <ManageOrder />
      //             </div>
      //           ) : this.state.value === "product" ? (
      //             <div>
      //               <ManageProduct />
      //             </div>
      //           ) : this.state.value === "orderReport" ? (
      //             <div>
      //               <OrderReport />
      //             </div>
      //           ) : this.state.value === "saleReport" ? (
      //             <div>
      //               <SaleReport />
      //             </div>
      //           ) : this.state.value === "transReport" ? (
      //             <div>
      //               <TransReport />
      //             </div>
      //           ) : this.state.value === "receiveReport" ? (
      //             <div>
      //               <ReceiveReport />
      //             </div>
      //           ) : null}
      //         </Col>
      //       </Row>
      //     </div>
      //   </>
      // </div>
    );
  }
}
