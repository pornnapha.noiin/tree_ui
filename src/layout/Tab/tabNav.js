import React, { Component } from "react";
import Tab from "react-bootstrap/Tab";
import "bootstrap/dist/css/bootstrap.min.css";
import "./tabNav.scss";
import { Nav, Container, Row, Col, Sonnet, Navbar } from "react-bootstrap";
import logoTreeSale from "../../assets/images/mytree.jpg";

export default class TabNav extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userName: localStorage.getItem("userName"),
      passWord: localStorage.getItem("passWord"),
      userType: localStorage.getItem("userType"),
    };
    this.logout = this.logout.bind(this);
  }

  logout = async () => {
    localStorage.clear();
    window.location.href = "/login";
  };

  render() {
    return (
      <Row style={{ margin: "0px", backgroundColor: "rgba(248,249,250,1)", }}>
        <div style={{ alignItems: "center", display: "flex", padding: "0px" }}>
          <img
            alt=""
            src={logoTreeSale}
            className="d-inline-block align-top"
            style={{
              // margin: "0 10px 0 20px",
              borderRadius: "50%",
              width: "auto",
              height: "10vh"
            }}
          /> Online Tree Sale
          {!this.state.userName ? (
            <Nav className="me-auto">
              <Nav.Link href="/login">เข้าสู่ระบบ</Nav.Link>
              <Nav.Link href="/register">สมัครสมาชิก</Nav.Link>
            </Nav>
          ) : (
            <Nav className="me-auto">
              <Nav.Link onClick={this.logout}>ออกจากระบบ</Nav.Link>
            </Nav>
          )}
        </div>
      </Row>
      // <div>
      //   <>
      //     <div>
      //       <Navbar className="nav-fix" bg="light" variant="light">
      //         <Container>
      //           <Navbar.Brand href="/">
      //             <img
      //               alt=""
      //               src={logoTreeSale}
      //               className="d-inline-block align-top"
      //               style={{
      //                 margin: "0 10px 0 20px",
      //                 borderRadius: "20px",
      //                 width: "2.5rem",
      //               }}
      //             />
      //             Online Tree Sale
      //           </Navbar.Brand>
      //           {!this.state.userName ? (
      //             <Nav className="me-auto">
      //               <Nav.Link href="/login">เข้าสู่ระบบ</Nav.Link>
      //               <Nav.Link href="/register">สมัครสมาชิก</Nav.Link>
      //             </Nav>
      //           ) : (
      //             <Nav className="me-auto">
      //               <Nav.Link onClick={this.logout}>ออกจากระบบ</Nav.Link>
      //             </Nav>
      //           )}
      //         </Container>
      //       </Navbar>
      //     </div>
      //   </>
      // </div>
    );
  }
}
