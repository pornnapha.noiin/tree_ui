import React, { Component } from "react";
import { Route, BrowserRouter } from "react-router-dom";

import Home from "./General/Home/home";
import Register from "./Register/register";
import Login from "./Login/login";
import Detail from "./General/Detail/detail";

import { HomeAdmin, Manage, ProductHome, EditProduct, SaveProduct, EditMember, EditOrder, SaveCategory } from 'Admin'

export default class route extends Component {
  constructor(props) {
    super(props)
  }

  render() {
    const pages = [
      { path: "/", component: Home },
      { path: "/register", component: Register },
      { path: "/login", component: Login },
      { path: "/detail", component: Detail },
      { path: "/admin/home", component: HomeAdmin },
      { path: "/admin/product/manage", component: Manage },
      { path: "/admin/product/home", component: ProductHome },
      { path: "/admin/product/edit", component: EditProduct },
      { path: "/admin/product/save", component: SaveProduct },
      { path: "/admin/member/edit", component: EditMember },
      { path: "/admin/order/check", component: EditOrder },
      { path: "/admin/category/save", component: SaveCategory },
    ]

    return (
      <BrowserRouter >
        {pages?.map((page, i) => (
          <Route key={i} exact path={page.path} component={page.component} />
        ))}
      </BrowserRouter>
    );
  }
}
